package com.example.aksha.project_doc;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SearchView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by shiva on 23-01-2019.
 */

public class searchActivity extends AppCompatActivity {
    ListView listView;
    ArrayList list;
    ArrayList visibleList;
    ArrayAdapter<String> adapter;
    SearchView searchView;
    AlertDialog alertDialog;
    String filterSelected = "both";
    String[] filterOptions;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_listlayout);
        //onSearchRequested();

        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));

        Toolbar toolbar = findViewById(R.id.searchtoolbar);
        //setSupportActionBar(toolbar);

        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // Get the intent, verify the action and get the query
        Log.d("Search", "New intent search Activity oncreate wala handle    ");
        handleIntent(getIntent());

        filterOptions = getResources().getStringArray(R.array.filter_search_options);

        listView = findViewById(R.id.search_listView);

        list = new ArrayList<>();
        visibleList = new ArrayList<>();


       /*
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,visiblelist);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                searchView.setQuery(parent.getItemAtPosition(position).toString(),true);
            }

        });*/
        createSearchBar();
        loadFilterDialogBox(getWindow().findViewById(R.id.search_listView));

    }

    /**
     * ?android:attr/actionModeWebSearchDrawable
     */

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        Log.d("Search", "New intent search Activity search    ");
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            Log.d("Search", "Same Activity search String queried is " + query);
            sendStringToShow(query);
        }
    }

    private void sendStringToShow(String query) {
        Log.d("Search", "sendStringtoShow success  ");
        /*Intent ins=new Intent(SearchableActivity.this,ProductList.class);
        ins.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        ins.addCategory("SearchActivity");
        ins.putExtra("Activity","SearchActivity");
        ins.putExtra("Query",query);
        startActivity(ins);*/
    }

    public void HomeAsUp(View v) {
        Log.d("Search", "Search Backpress button is clicked ");
        onBackPressed();
    }


    private void doMySearch(String query) {
        Log.e("Search", "String queried is " + query);
    }

    private void createSearchBar() {
        // Get the SearchView and set the searchable configuration
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) findViewById(R.id.tempsearchview);
        // Assumes current activity is the searchable activity
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(false); // Do not iconify the widget; expand it by default

        // searchView.setFocusable(true);
        searchView.setMaxWidth(Integer.MAX_VALUE);
        searchView.onActionViewExpanded();
        /*searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchView.clearFocus();

                Log.d("Search", "Search Submit button is clicked "+query);
             /*   if(list.contains(query)){
                    adapter.getFilter().filter(query);
                }else{
                    Toast.makeText(MainActivity.this, "No Match found",Toast.LENGTH_LONG).show();
                }*
                return false;

            }

            @Override
            public boolean onQueryTextChange(String newText) {
                Log.d("Search", "Search text length   "+newText.length());
                if(newText.length()>0){
                    visiblelist.addAll(list);
                    adapter.getFilter().filter(newText);
                    Log.d("Search", "list   "+visiblelist);
                    Log.d("Search", "list   "+list);
                }else{
                    visiblelist.clear();
                    adapter.getFilter().filter("MayankMobileCare");

                    Log.d("Search", "list   "+visiblelist);
                    Log.d("Search", "list   "+list);
                }

                return false;
            }
        });*/

        /** //remove search icon  or set @null in view
         int magId = getResources().getIdentifier("android:id/search_mag_icon", null, null);
         ImageView magImage = (ImageView) searchView.findViewById(magId);
         magImage.setLayoutParams(new LinearLayout.LayoutParams(0, 0));*/
    }

    //Handling logout dialog box
    public void loadFilterDialogBox(View v) {

        //For the Dialog . . .
        try {
            LayoutInflater inflater = LayoutInflater.from(this);
            @SuppressLint("InflateParams") final View dialogView = inflater.inflate(R.layout.searchfilter_dialog, null);
            alertDialog = new AlertDialog.Builder(this).create();
            alertDialog.setView(dialogView);
            alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            Window window = alertDialog.getWindow();
            WindowManager.LayoutParams lp = alertDialog.getWindow().getAttributes();
            lp.dimAmount = .7f;
            alertDialog.getWindow().setAttributes(lp);
            alertDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            //window.setGravity(Gravity.CENTER);

            //dialog building
            if (window != null) {
                window.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            }
            if (window != null) {
                window.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            }

            RadioButton r1 = dialogView.findViewById(R.id.DocFilterButton);
            RadioButton r2 = dialogView.findViewById(R.id.HospitalFilterButton);
            RadioButton r3 = dialogView.findViewById(R.id.BothFilterButton);
            r1.setText(filterOptions[0]);
            r2.setText(filterOptions[1]);
            r3.setText(filterOptions[2]);

            RadioGroup radioGroup =  dialogView.findViewById(R.id.sortoptionradioGroup);
            radioGroup.clearCheck();

            radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @SuppressLint("ResourceType")
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    RadioButton rb =  group.findViewById(checkedId);
                    if (null != rb && checkedId > -1) {

                        filterSelected = rb.getText().toString().toLowerCase();
                        Toast.makeText(searchActivity.this, filterSelected, Toast.LENGTH_SHORT).show();

                    }
                    alertDialog.dismiss();
                }
            });

            dialogView.findViewById(R.id.cancelDialog).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                }
            });

        } catch (Exception e) {
            Log.d("MAINACTIVITY", String.valueOf(e));
        }
        //showing dialog when the button is clicked . . .
        alertDialog.show();

    }

 /*   @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.searchablemenu, menu);
        // getMenuInflater().inflate(R.layout.custom_actionbar,menu);

        // Get the SearchView and set the searchable configuration
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        final SearchView searchView = (SearchView) menu.findItem(R.id.ssearchbtn).getActionView();
        // Assumes current activity is the searchable activity
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(false); // Do not iconify the widget; expand it by default

        // searchView.setFocusable(true);
        //searchView.setMaxWidth( Integer.MAX_VALUE );

           LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT);
           params.leftMargin=-5;
         searchView.setLayoutParams(params);

        LinearLayout searchEditFrame = (LinearLayout) searchView.findViewById(R.id.search_edit_frame); // Get the Linear Layout
        // Get the associated LayoutParams and set leftMargin
        ((LinearLayout.LayoutParams) searchEditFrame.getLayoutParams()).leftMargin = 0;

        searchView.onActionViewExpanded();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchView.clearFocus();
             /*   if(list.contains(query)){
                    adapter.getFilter().filter(query);
                }else{
                    Toast.makeText(MainActivity.this, "No Match found",Toast.LENGTH_LONG).show();
                }*
                return false;

            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return false;
            }
        });
        // return super.onCreateOptionsMenu(menu);
        //menu.findItem(R.id.searchbtn2).getActionView().setVisibility(View.GONE);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.v("MainActivity", "Backpress button is clicked id "+item.getItemId()+" "+android.R.id.home);
        if(item.getItemId()==android.R.id.home){
            Log.v("MainActivity", "Search Backpress button is clicked ");
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }*/


}
