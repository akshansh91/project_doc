package com.example.aksha.project_doc.model;

import java.util.Date;

/**
 * Created by shiva on 10-01-2019.
 */

public class WishItem {
    public int ID;
    public String Category;
    public String ProductId;
    public String date;

    public WishItem(){}
    public WishItem(int ID, String category, String productId, String date) {
        this.ID = ID;
        Category = category;
        ProductId = productId;
        this.date = date;
    }

    public WishItem(String category, String productId, String date) {
        Category = category;
        ProductId = productId;
        this.date = date;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }

    public String getProductId() {
        return ProductId;
    }

    public void setProductId(String productId) {
        ProductId = productId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
