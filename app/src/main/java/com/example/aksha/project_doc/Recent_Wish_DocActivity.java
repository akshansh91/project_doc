package com.example.aksha.project_doc;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.aksha.project_doc.Database.DatabaseHandler;
import com.example.aksha.project_doc.model.WishItem;
import com.example.aksha.project_doc.utils.ServerConfig;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by shivam on 27-04-2019.
 */
public class Recent_Wish_DocActivity extends Fragment {
    private RecyclerView mRecyclerView;
    Intent in;
    String activity;
    DatabaseHandler db;
    private ArrayList recentProductlist = new ArrayList<>();
    private ArrayList<String>  recentproductidlist=new ArrayList<>();
    WishItem wishitem;
    private ArrayList<String>  wishlist=new ArrayList<>();
    List<WishItem> RecentList;
    DatabaseHandler recdb;
    View v;
    Context context;

    String category, responseString, TAG = "Recent_Wish";
    Toolbar toolbar;
    //RecyclerView viewDoctorList;

    OkHttpClient client;
    HttpUrl.Builder builder;

    Request request;
    Response response;

    ProgressDialog progressDialog;

    ArrayList<String> list;
    ArrayList<ArrayList<String>> finalList;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*setContentView(R.layout.activity_display_doctors);

        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));

        in=getIntent();
        activity=in.getStringExtra("Activity");

        Toolbar toolbar = findViewById(R.id.toolbarDisplayDoctors);
        try {
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

        finalList = new ArrayList<>();

        mRecyclerView = findViewById(R.id.doctorList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(Recent_Wish_DocActivity.this, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(linearLayoutManager);

        progressDialog = new ProgressDialog(Recent_Wish_DocActivity.this);
        progressDialog.setTitle("Loading");
        progressDialog.setMessage("Please Wait!!");
        progressDialog.setIndeterminate(false);
        progressDialog.setCancelable(false);

        db=new DatabaseHandler(this);
        if(activity.equals("RecentActivity")){
            //getSupportActionBar().setTitle("Recently viewed");
            toolbar.setTitle("Recent");
            setRecentlyViewList();
        }else{
           // getSupportActionBar().setTitle("WishList");
            toolbar.setTitle("Favourite");
            //setWishViewList();
        }
        */

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v=inflater.inflate(R.layout.activity_display_doctors, container, false);
        context=getContext();

        activity=getArguments().getString("Activity");

        Toolbar toolbar = v.findViewById(R.id.toolbarDisplayDoctors);
        toolbar.setVisibility(View.GONE);

        finalList = new ArrayList<>();

        mRecyclerView = v.findViewById(R.id.doctorList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(linearLayoutManager);

        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("Loading");
        progressDialog.setMessage("Please Wait!!");
        progressDialog.setIndeterminate(false);
        progressDialog.setCancelable(false);

        db=new DatabaseHandler(getContext());
        if(activity.equals("RecentActivity")){
            //getSupportActionBar().setTitle("Recently viewed");
            //toolbar.setTitle("Recent");
            setRecentlyViewList();
        }else{
            // getSupportActionBar().setTitle("WishList");
            //toolbar.setTitle("Favourite");
            setWishViewList();
        }
        return v;
    }

    public Adapter_WishDocList mWishAdapter;
    private void setWishViewList() {
        Log.d("MainActivity", "setWiahliat ViewList added : start 0 ");
        final DatabaseHandler recdb=new DatabaseHandler(context);
        //mWishAdapter = new WishRecyclerViewAdapter(this,(ArrayList) recentProductlist,this);
        mWishAdapter = new Adapter_WishDocList(context, finalList, new Adapter_WishDocList.clickListener() {
            @Override
            public void onItemClicked(String id) {
                Intent intent = new Intent(context, viewDoctorProfile.class);
                intent.putExtra("id", id);
                Log.d("displayDoctors", "id :" + id);
                startActivity(intent);
            }
        });
        mRecyclerView.setAdapter(mWishAdapter);

        if(!recdb.getAllWish().isEmpty()) {
            RecentList = recdb.getAllWish();
            int limit = RecentList.size();

            Log.d("MainActivity", "setRecentlyViewList added : start 1 " + RecentList.size());
            for (int i = 0; i < limit; i++) {
                try {
                    wishitem = RecentList.get((RecentList.size() - i - 1));
                    String RecentCategory = wishitem.getCategory();
                    String Recent_Item_Id = wishitem.getProductId();

                    getDocDetails docDetails = new getDocDetails();
                    docDetails.execute(Recent_Item_Id);

                    Log.d("MainActivity", "Item Document Reference added :  " + Recent_Item_Id);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d("MainActivity", "Display all RecentItems in recycle view");
            }
            Log.d("MainActivity", "Display all WishItems");
           mWishAdapter.notifyDataSetChanged();
        }

    }



    /**     ----    Recently Viewed Section         ----            */

   Adapter_DocList adapterDocList;
   private void setRecentlyViewList() {
        Log.d("Recent_Wish", "setRecentlyViewList added : start 0 ");
        recdb=new DatabaseHandler(context);

        adapterDocList = new Adapter_DocList(context, finalList, new Adapter_DocList.clickListener() {
           @Override
           public void onItemClicked(String id) {
               Intent intent = new Intent(context, viewDoctorProfile.class);
               intent.putExtra("id", id);
               Log.d("displayDoctors", "id :" + id);
               startActivity(intent);
           }
       });
       mRecyclerView.setAdapter(adapterDocList);



        if(!recdb.getAllRecent().isEmpty()) {
            RecentList = recdb.getAllRecent();
            int limit = 0;
            if (RecentList.size() > 15) {
                clearExcessRecents();
                limit = 15;
                RecentList = recdb.getAllRecent();
            } else {
                limit = RecentList.size();
            }

            Log.d("Recent_Wish", "setRecentlyViewList added : start 1 " + RecentList.size());
            for (int i = 0; i < limit; i++) {
                try {
                    wishitem = RecentList.get((RecentList.size() - i - 1));
                    String RecentCategory = wishitem.getCategory();
                    String Recent_Item_Id = wishitem.getProductId();

                    getDocDetails docDetails = new getDocDetails();
                    docDetails.execute(Recent_Item_Id);

                    Log.d("Recent_Wish", "Item Document Reference added id :  " + Recent_Item_Id);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d("Recent_Wish", "Display all RecentItems in recycle view");
            }
            adapterDocList.notifyDataSetChanged();
            Log.d("Recent_Wish", "Display all RecentItems");

        }

    }
    private void clearExcessRecents() {
        final DatabaseHandler recdb = new DatabaseHandler(context);
        if (!recdb.getAllRecent().isEmpty()) {
            List<WishItem> mRecentList = recdb.getAllRecent();
            int extra=recdb.getAllRecent().size()-10;
            for (int i = 0; i < extra; i++) {
                db.deleteRecentItem(mRecentList.get(i));
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    class getDocDetails extends AsyncTask<String, String, String> {
        String id;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
             id = strings[0];

            client = new OkHttpClient.Builder().connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS).build();

            builder = Objects.requireNonNull(HttpUrl.parse(new ServerConfig().getDoctorDetailsUsingID())).newBuilder();
            builder.addQueryParameter("id", id);

            String url = builder.build().toString();

            request = new Request.Builder().url(url).build();

            try {
                response = client.newCall(request).execute();
            } catch (Exception e) {
                Log.d(TAG, "inside getDoctorsDetails doInBackground() exe_1: " + e);
            }

            if (response.body() != null) {
                try {
                    responseString = response.body().string();
                } catch (Exception e) {
                    Log.d(TAG, "inside getDoctorsDetails doInBackground() exe_2: " + e);
                }
            }
            Log.d(TAG, "inside getDoctorsDetails doInBackground() responseString: " + responseString);

            return responseString;
        }

        @Override
        protected void onPostExecute(String s) {
            progressDialog.dismiss();
            if (!s.equals("error")) {
                Log.d(TAG, "inside getDoctorsDetails onPostExecute() going to process response");
                processResponse(s,id);
            } else {
                recdb.deleteRecentItem(wishitem);
                Toast.makeText(context, ".........", Toast.LENGTH_SHORT).show();
        }
        }
    }

    private void processResponse(String s,String id) {
        //String s=data[0];
        //adding data in different array lists ...
        String chunkOfData[] = s.split("___");

        int counter = 1;
        int size = chunkOfData.length;

        while (counter < size) {
            list = new ArrayList<>();

            String blockOfData = chunkOfData[counter];
            String[] array = blockOfData.trim().split("\\*",-1);

            //name,degree,phone,address,timingM,timingE,m,tu,wed,th,fr,sat,su,note


            String name = array[0];
            list.add(name);
            Log.d("API_Call", "name: " + name);

            String degree = array[1];
            list.add(degree);
            Log.d("API_Call", "degree: " + degree);

            String phone = array[3];
            list.add(phone);
            Log.d("API_Call", "phone: " + phone);

            String address = array[4];
            list.add(address);
            Log.d("API_Call", "address: " + address);

            String timingM = array[5];
            list.add(timingM);
            Log.d("API_Call", "timingM: " + timingM);

            String timingE = array[6];
            list.add(timingE);
            Log.d("API_Call", "timingE: " + timingE);

            String m = array[7];
            list.add(m);
            Log.d("API_Call", "m: " + m);

            String tu = array[8];
            list.add(tu);
            Log.d("API_Call", "tu: " + tu);

            String wed = array[9];
            list.add(wed);
            Log.d("API_Call", "wed: " + wed);

            String th = array[10];
            list.add(th);
            Log.d("API_Call", "th: " + th);

            String fr = array[11];
            list.add(fr);
            Log.d("API_Call", "fr: " + fr);

            String sat = array[12];
            list.add(sat);
            Log.d("API_Call", "sat: " + sat);

            String sun = array[13];
            list.add(sun);
            Log.d("API_Call", "sun: " + sun);

            /*String fees = array[13];
            list.add(fees);
            Log.d("API_Call", "fees: " + fees);*/

            String note = array[14];
            list.add(note);
            Log.d("API_Call", "note: " + note);

            //String id = id;
            list.add(id);
            Log.d("API_Call", "id: " + id);

            Log.d("Recent_Wish", "List " + counter + ":" + list);
            finalList.add(list);
            counter++;
        }

        if(activity.equals("RecentActivity")){
            adapterDocList.notifyDataSetChanged();
        }else{
            mWishAdapter.notifyDataSetChanged();
        }

    }


}
