package com.example.aksha.project_doc;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class Adapter_Doc_Category_RecyclerView extends RecyclerView.Adapter<Adapter_Doc_Category_RecyclerView.MyViewHolder> {

    public interface clickListener {
        void onItemClicked(String category);
    }

    private Context context;
    private ArrayList<String> data;
    private LayoutInflater inflater;
    private clickListener myClickListener;

    public Adapter_Doc_Category_RecyclerView(Context context, ArrayList<String> data, clickListener clickListener) {
        this.context = context;
        this.data = data;
        inflater = LayoutInflater.from(context);
        myClickListener = clickListener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        Log.d("package", context.getPackageName());
        View view = inflater.inflate(R.layout.category_card_recycler_view, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        //String category = data.get(i).toLowerCase();
        //String category = data.get(i).toUpperCase();

        //String category=data.get(i).substring(0, 1).toUpperCase() + data.get(i).substring(1);
        String category2=properCapitalise(data.get(i));

        myViewHolder.bind(category2, myClickListener);
        myViewHolder.category.setText(category2);

        myViewHolder.bg.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.blueoutline_round_bg));
    }

    public String properCapitalise(String string){

        String category="";
        String[] array = string.split(" ");
        for(int k=0;k<array.length;k++){
            category= new StringBuilder().append(category).append(array[k].substring(0, 1).toUpperCase()).append(array[k].substring(1)).append(" ").toString();
        }

        String category2="";
        try {
            String[] array2 = category.split("\\.");
            category2=array2[0];
            for(int k=1;k<array2.length;k++){
                category2= new StringBuilder().append(category2).append(".").append(array2[k].substring(0, 1).toUpperCase()).append(array2[k].substring(1)).toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return category2;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView category;
        RelativeLayout bg;

        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            category = itemView.findViewById(R.id.category);
            bg = itemView.findViewById(R.id.cdcontainer1);
        }

        public void bind(final String item, final clickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClicked(item);
                }
            });
        }
    }
}
