package com.example.aksha.project_doc;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class Adapter_DocList extends RecyclerView.Adapter<Adapter_DocList.MyViewHolder> {

    public interface clickListener {
        void onItemClicked(String image);
    }

    private Context context;
    private ArrayList<ArrayList<String>> data;
    private LayoutInflater inflater;
    /*private int mExpandedPosition = -1;
    private int pos;*/
    public clickListener myClickListener;

    public Adapter_DocList(Context context, ArrayList<ArrayList<String>> data, clickListener clickListener) {
        this.context = context;
        this.data = data;
        inflater = LayoutInflater.from(context);
        myClickListener = clickListener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = inflater.inflate(R.layout.doc_list_card, viewGroup, false);
        return new MyViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        //specifying the elements from the list to display in the list ...
        //according to a particular position ...

        //name,degree,phone,address,timingM,timingE,m,tu,wed,th,fr,sat,su,note

        //pos = myViewHolder.getAdapterPosition();

        /*final boolean isExpanded = i == mExpandedPosition;
        myViewHolder.subItem.setVisibility(isExpanded ? View.VISIBLE : View.GONE);
        myViewHolder.itemView.setActivated(isExpanded);

        myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mExpandedPosition = isExpanded ? -1 : pos;
                notifyDataSetChanged();
            }
        });*/

        String docName = data.get(i).get(0);
        String docDegree = data.get(i).get(1);
        final String docNumber = data.get(i).get(2);
        String docAddress = data.get(i).get(3);
        String timingM = data.get(i).get(4);
        String timingE = data.get(i).get(5);
        int m = Integer.parseInt(data.get(i).get(6));
        int tu = Integer.parseInt(data.get(i).get(7));
        int wed = Integer.parseInt(data.get(i).get(8));
        int th = Integer.parseInt(data.get(i).get(9));
        int fr = Integer.parseInt(data.get(i).get(10));
        int sat = Integer.parseInt(data.get(i).get(11));
        int sun = Integer.parseInt(data.get(i).get(12));
        String note = data.get(i).get(13);
        String id = data.get(i).get(14);

        myViewHolder.bind(id, myClickListener);

        myViewHolder.docDegree.setText(docDegree.replace(","," , "));
        myViewHolder.docNumber.setText(docNumber);
        myViewHolder.docName.setText(properCapitalise(docName));
        myViewHolder.docAddress.setText(docAddress);
        myViewHolder.timeMorning.setText(timingM);
        myViewHolder.timeEvening.setText(timingE);
        myViewHolder.note.setText(context.getString(R.string.note) + note);

        if (m == 1) {
            myViewHolder.monday.setTextColor(context.getResources().getColor(R.color.colorAccent));
        } /*else {
            //myViewHolder.monday.setImageResource(docName);
        }*/
        if (tu == 1) {
            myViewHolder.tuesday.setTextColor(context.getResources().getColor(R.color.colorAccent));
        } /*else {
            //myViewHolder.tuesday.setImageResource(docName);
        }*/
        if (wed == 1) {
            myViewHolder.wednesday.setTextColor(context.getResources().getColor(R.color.colorAccent));
        } /*else {
            myViewHolder.monday.setTextColor(context.getResources().getColor(R.color.colorAccent));
        }*/
        if (th == 1) {
            myViewHolder.thursday.setTextColor(context.getResources().getColor(R.color.colorAccent));
        } /*else {
            //myViewHolder.thursday.setImageResource(docName);
        }*/
        if (fr == 1) {
            myViewHolder.friday.setTextColor(context.getResources().getColor(R.color.colorAccent));
        } /*else {
            //myViewHolder.friday.setImageResource(docName);
        }*/
        if (sat == 1) {
            myViewHolder.saturday.setTextColor(context.getResources().getColor(R.color.colorAccent));
        }/* else {
            //myViewHolder.saturday.setImageResource(docName);
        }*/
        if (sun == 1) {
            myViewHolder.sunday.setTextColor(context.getResources().getColor(R.color.colorAccent));
        } /*else {
            //myViewHolder.sunday.setImageResource(docName);
        }*/

        myViewHolder.call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", docNumber, null));
                context.startActivity(intent);
            }
        });
    }
    public String properCapitalise(String string){

        String category="";
        String[] array = string.split(" ");
        for(int k=0;k<array.length;k++){
            category= new StringBuilder().append(category).append(array[k].substring(0, 1).toUpperCase()).append(array[k].substring(1).toLowerCase()).append(" ").toString();
        }

        String category2="";
        try {
            String[] array2 = category.split("\\.");
            category2=array2[0];
            for(int k=1;k<array2.length;k++){
                category2= new StringBuilder().append(category2).append(".").append(array2[k].substring(0, 1).toUpperCase()).append(array2[k].substring(1)).toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return category2;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView docImage;
        TextView monday, tuesday, wednesday, thursday, friday, saturday, sunday;
        TextView docName, docNumber, docDegree, timeMorning, timeEvening, call, fees;
        ConstraintLayout subItem;
        EditText docAddress, note;

        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            docImage = itemView.findViewById(R.id.doctorImage);
            docName = itemView.findViewById(R.id.docName);
            docNumber = itemView.findViewById(R.id.docNumber);
            docDegree = itemView.findViewById(R.id.docDegree);
            subItem = itemView.findViewById(R.id.subItem);
            docAddress = itemView.findViewById(R.id.docAddress);
            timeEvening = itemView.findViewById(R.id.timeEvening);
            timeMorning = itemView.findViewById(R.id.timeMorning);
            call = itemView.findViewById(R.id.call);
            monday = itemView.findViewById(R.id.monday);
            tuesday = itemView.findViewById(R.id.tuesday);
            wednesday = itemView.findViewById(R.id.wednesday);
            thursday = itemView.findViewById(R.id.thursday);
            friday = itemView.findViewById(R.id.friday);
            saturday = itemView.findViewById(R.id.saturday);
            sunday = itemView.findViewById(R.id.sunday);
            fees = itemView.findViewById(R.id.fees);
            note = itemView.findViewById(R.id.note);
        }

        public void bind(final String item, final clickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClicked(item);
                }
            });
        }

    }
}
