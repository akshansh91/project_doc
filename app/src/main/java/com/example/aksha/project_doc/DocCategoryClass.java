package com.example.aksha.project_doc;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import java.util.ArrayList;

public class DocCategoryClass extends AppCompatActivity {

    RecyclerView recyclerView;
    Adapter_Doc_Category adapter_doc_category;
    ArrayList<String> name;
    ArrayList<Integer> imageId;
    String TAG_ = "CLASS";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_category);
        Log.d(TAG_, "DocCategoryClass");
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        name = new ArrayList<>();
        //by patel
        name.add("haematologist");
        name.add("medicine");
        name.add("neuro physician");
        name.add("m.d. physician");
        name.add("microbiologist");
        name.add("maternity and child health");
        name.add("neuro surgeon");
        name.add("oncology cancer");
        name.add("nephrologist");
        name.add("neurologist");
        name.add("opthamology");
        name.add("orthopaedic");
        name.add("obct");
        name.add("occupational therapist");
        name.add("paediatrician");

        //by sharma
        name.add("dentist");
        name.add("critical care specialist");
        name.add("cardio thoracic surgeon");
        name.add("cardiology");
        name.add("consultant physician");
        name.add("audiologist");
        name.add("ayurvedic");
        name.add("occupational theraoist");

        imageId = new ArrayList<>();
        imageId.add(R.drawable.haematologist);//clear
        imageId.add(R.drawable.medicine_1);//clear
        imageId.add(R.drawable.neurologist_1);//clear
        imageId.add(R.drawable.physician_1);//clear
        imageId.add(R.drawable.microbiologist);//clear
        imageId.add(R.drawable.gynecologist);//clear
        imageId.add(R.drawable.neurologist_1);//clear
        imageId.add(R.drawable.oncologist);//clear
        imageId.add(R.drawable.nephrology_1);//clear
        imageId.add(R.drawable.neurologist_1);//clear
        imageId.add(R.drawable.ophthalmologist_1);//clear
        imageId.add(R.drawable.orthopedic);//clear
        imageId.add(R.drawable.gastroenterology_1);//clear
        imageId.add(R.drawable.ot_1);//clear
        imageId.add(R.drawable.pediatrician_1);//clear

        //by sharma
        imageId.add(R.drawable.dentist);//clear
        imageId.add(R.drawable.critical_care_1);//clear
        imageId.add(R.drawable.cardiothoraric);//clear
        imageId.add(R.drawable.cardiology);//clear
        imageId.add(R.drawable.physician_1);//clear
        imageId.add(R.drawable.audiologist_1);//clear
        imageId.add(R.drawable.ayurvedic_1);//clear
        imageId.add(R.drawable.therapist_1);//clear

        recyclerView = findViewById(R.id.recyclerView);

        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(DocCategoryClass.this, 2);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecorator(2, dpToPx(), true));

        adapter_doc_category = new Adapter_Doc_Category(DocCategoryClass.this, name, imageId, new Adapter_Doc_Category.clickListener() {
            @Override
            public void onItemClicked(String docCategory) {
                Intent intent = new Intent(DocCategoryClass.this, displayDoctors.class);
                intent.putExtra("category", docCategory);
                Log.d("DocCategoryClass", "category :" + docCategory);
                startActivity(intent);
            }
        });

        recyclerView.setAdapter(adapter_doc_category);
    }

    private int dpToPx() {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, r.getDisplayMetrics()));
    }

    public class GridSpacingItemDecorator extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecorator(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            Log.v("MainActivity", "Backpress button is clicked ");
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
