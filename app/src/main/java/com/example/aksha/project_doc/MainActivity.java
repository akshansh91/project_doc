package com.example.aksha.project_doc;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;

import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.util.List;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private String TAG = "MainActivity";
    Button searchDocs, searchHospitals, lookDocsAround;
    /*double latitude, longitude;
    trackerGPS trackerGPS;*/
    LocationManager manager;

    DrawerLayout drawer;
    private String[] activityTitles;
    NavigationView navigationView;

    // index to identify current nav menu item
    public static int navItemIndex = 0;
    private Handler mHandler;
    // tags used to attach the fragments
    private static final String TAG_HOME = "Home", TAG_FAV = "Favourite", TAG_HOSPITAL = "Hospital", TAG_LOCATION = "Location";
    private static final String TAG_RECENT = "Recent", TAG_CALL = "Ambulance";
    public static String CURRENT_TAG = TAG_HOME;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main1);

        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));

        checkLocationPermission();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED &&
                    checkSelfPermission(Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED &&
                    checkSelfPermission(Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_CONTACTS, Manifest.permission.ACCESS_FINE_LOCATION,
                                Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.SEND_SMS,
                                Manifest.permission.CALL_PHONE}, 1);
            }
        }
        manager = (LocationManager) this.getSystemService(LOCATION_SERVICE);
        boolean isEnabledGPS = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean isEnabledNetwork = manager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        if (!isEnabledGPS && !isEnabledNetwork) {
            showSettingAlert();
        }

       /* searchDocs = findViewById(R.id.searchDoctors);
        searchHospitals = findViewById(R.id.searchHospitals);
        lookDocsAround = findViewById(R.id.lookForDoctorsNearYou);

        //trackerGPS = new trackerGPS(this);

        searchDocs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, DocCategoryClass.class);
                startActivity(intent);
            }
        });

        searchHospitals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, hospitalsListClass.class);
                startActivity(intent);
            }
        });

        lookDocsAround.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, lookDoctorsAroundClass.class);
                startActivity(intent);
            }
        });*/

        // load toolbar titles from string resources
        activityTitles = getResources().getStringArray(R.array.nav_item_activity_titles);
        mHandler = new Handler();


        Toolbar toolbar = findViewById(R.id.toolbarMainActivity);
        setSupportActionBar(toolbar);

        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        toggle.setDrawerIndicatorEnabled(false);
        toggle.setHomeAsUpIndicator(R.drawable.ic_toggle_white);
        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawer.isDrawerVisible(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        if (savedInstanceState == null) {
            navItemIndex = 0;
            CURRENT_TAG = TAG_HOME;
            loadHomeFragment();

        }
    }

    private void loadHomeFragment() {
        // selecting appropriate nav menu item
        selectNavMenu();

        // set toolbar title
        setToolbarTitle();

        // if user select the current navigation menu again, don't do anything
        // just close the navigation drawer
        if (getSupportFragmentManager().findFragmentByTag(CURRENT_TAG) != null) {
            drawer.closeDrawers();
            return;
        }

        // Sometimes, when fragment has huge data, screen seems hanging
        // when switching between navigation menus
        // So using runnable, the fragment is loaded with cross fade effect
        // This effect can be seen in GMail app
        Runnable mPendingRunnable = new Runnable() {
            @Override
            public void run() {
                // update the main content by replacing fragments
                Fragment fragment = getHomeFragment();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                        android.R.anim.fade_out);
                fragmentTransaction.replace(R.id.frame, fragment, CURRENT_TAG);
                fragmentTransaction.commitAllowingStateLoss();
            }
        };
        // If mPendingRunnable is not null, then add to the message queue
        if (mPendingRunnable != null) {
            mHandler.post(mPendingRunnable);
        }
        //Closing drawer on item click
        drawer.closeDrawers();

        // refresh toolbar menu
        invalidateOptionsMenu();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        if (requestCode == 1) {
            boolean isPermissionForAllGranted = false;
            if (grantResults.length > 0 && permissions.length == grantResults.length) {
                for (int i = 0; i < permissions.length; i++) {
                    isPermissionForAllGranted = grantResults[i] == PackageManager.PERMISSION_GRANTED;
                }
            } else {
                isPermissionForAllGranted = true;
            }
            if (isPermissionForAllGranted) {
                Toast.makeText(MainActivity.this, "Permission granted", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private Fragment getHomeFragment() {
        switch (navItemIndex) {
            case 0:
                // home
                HomeCategoryFragment homeFragment = new HomeCategoryFragment();
                return homeFragment;
            case 1:
                Bundle bd = new Bundle();
                bd.putString("Activity", "FavouriteActivity");
                Recent_Wish_DocActivity Favouritefragment = new Recent_Wish_DocActivity();
                Favouritefragment.setArguments(bd);
                return Favouritefragment;
            case 2:
                Bundle bn = new Bundle();
                bn.putString("Activity", "RecentActivity");
                Recent_Wish_DocActivity recentfragment = new Recent_Wish_DocActivity();
                recentfragment.setArguments(bn);
                return recentfragment;
            default:
                return new HomeCategoryFragment();
        }
    }

    private void setToolbarTitle() {
        getSupportActionBar().setTitle(activityTitles[navItemIndex]);
    }

    private void selectNavMenu() {
        navigationView.getMenu().getItem(navItemIndex).setChecked(true);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    private void showSettingAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        alertDialog.setTitle("Start GPS");

        alertDialog.setMessage("GPS not enabled!!!");

        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        });

        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        alertDialog.show();
    }


    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            new MaterialDialog.Builder(MainActivity.this)
                    .title(getString(R.string.location_permission))
                    .content("Allow us to access your permission for better experience")
                    .negativeText("Cancel")
                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                        }
                    })
                    .positiveText("Give Permission")
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            getPermissionsUsingDexter_Location();
                        }
                    })
                    .show();

        } else {
            Log.d(TAG, "Have Permission Already");
            //getLocation();
        }
    }

    /*private void getLocation() {
        if (trackerGPS.canGetLocation()) {
            latitude = trackerGPS.getLatitude();
            longitude = trackerGPS.getLongitude();
            Log.d("Location", "Latitude: " + latitude);
            Log.d("Location", "Longitude: " + longitude);
        } else {
            trackerGPS.showSettingAlert();
        }
    }*/

    private void getPermissionsUsingDexter_Location() {
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.ACCESS_FINE_LOCATION
                ).withListener(new MultiplePermissionsListener() {
            @Override
            public void onPermissionsChecked(MultiplePermissionsReport report) {
                if (report.areAllPermissionsGranted()) {
                    //getLocation();
                    Log.d(TAG, "permission given");
                } else {
                    Log.d(TAG, "permission not granted");
                }
            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {

            }
        }).check();
    }


   /* @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        // getMenuInflater().inflate(R.layout.custom_actionbar,menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        Log.d("MainActivity", "AppBar Menu Items clicked success  ");
        //noinspection SimplifiableIfStatement
        if (id == R.id.searchbtn2) {
            Log.d("MainActivity", "View Search Menu Items 2 clicked success  ");
            Intent inst = new Intent(MainActivity.this, searchActivity.class);
            inst.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(inst);
            //onSearchRequested();
        }

        return super.onOptionsItemSelected(item);
    }*/

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            navItemIndex = 0;
            CURRENT_TAG = TAG_HOME;
            loadHomeFragment();

            /*Intent intent = new Intent(MainActivity.this, DocCategoryClass.class);
            intent.putExtra("Activity","DocCategoryActivity");
            startActivity(intent);*/
        } else if (id == R.id.nav_favourite) {
            navItemIndex = 1;
            CURRENT_TAG = TAG_FAV;
            loadHomeFragment();

           /* Intent intent = new Intent(MainActivity.this, Recent_Wish_DocActivity.class);
            intent.putExtra("Activity","WishActivity");
            startActivity(intent);*/
        } else if (id == R.id.nav_recent) {
            navItemIndex = 2;
            CURRENT_TAG = TAG_RECENT;
            loadHomeFragment();

            /*Intent intent = new Intent(MainActivity.this, Recent_Wish_DocActivity.class);
            intent.putExtra("Activity","RecentActivity");
            startActivity(intent);*/
        } else if (id == R.id.nav_hospital) {
            Intent intent = new Intent(MainActivity.this, hospitalsListClass.class);
            startActivity(intent);
        } else if (id == R.id.nav_bylocation) {
            Intent intent = new Intent(MainActivity.this, lookDoctorsAroundClass.class);
            startActivity(intent);
        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_callambulance) {
            Intent intent = new Intent(Intent.ACTION_CALL, Uri.fromParts("tel", "108", null));
            startActivity(intent);
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
