package com.example.aksha.project_doc;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class Adapter_DocAround extends RecyclerView.Adapter<Adapter_DocAround.MyViewHolder> {

    public interface clickListener {
        void onItemClicked(String image);
    }

    private LayoutInflater inflater;
    private Context context;
    private ArrayList<ArrayList<String>> data;
    public clickListener myClickListener;

    public Adapter_DocAround(Context context, ArrayList<ArrayList<String>> data, clickListener clickListener) {
        this.context = context;
        this.data = data;
        inflater = LayoutInflater.from(context);
        myClickListener = clickListener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        Log.d("context", context.getPackageName());
        View view = inflater.inflate(R.layout.doc_around_card, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        //name,degree,phone,address,timingM,timingE,m,tu,wed,th,fr,sat,su,fees,note,distance,category,id ...
        String docName = data.get(i).get(0);
        String docDegree = data.get(i).get(1);
        final String docNumber = data.get(i).get(2);
        String docAddress = data.get(i).get(3);
        String timingM = data.get(i).get(4);
        String timingE = data.get(i).get(5);
        int m = Integer.parseInt(data.get(i).get(6));
        int tu = Integer.parseInt(data.get(i).get(7));
        int wed = Integer.parseInt(data.get(i).get(8));
        int th = Integer.parseInt(data.get(i).get(9));
        int fr = Integer.parseInt(data.get(i).get(10));
        int sat = Integer.parseInt(data.get(i).get(11));
        int sun = Integer.parseInt(data.get(i).get(12));
        //String fees = data.get(i).get(13);
        String note = data.get(i).get(13);
        String category = data.get(i).get(14).toLowerCase();
        String id = data.get(i).get(15);
        String distance = data.get(i).get(16);

        myViewHolder.bind(id, myClickListener);

        myViewHolder.docDegree.setText(docDegree.toUpperCase().replace("," , " , "));
        myViewHolder.docName.setText(properCapitalise(docName));
        myViewHolder.docSpecialization.setText( category.length()>0 ? properCapitalise(category): "");
        myViewHolder.docDistance.setText(distance);
        myViewHolder.docNumber.setText(docNumber.replace(","," , "));
    }


    public String properCapitalise(String string){

        String category="";
        String[] array = string.split(" ");
        for(int k=0;k<array.length;k++){
            category= new StringBuilder().append(category).append(array[k].substring(0, 1).toUpperCase()).append(array[k].substring(1).toLowerCase()).append(" ").toString();
        }

        String category2="";
        try {
            String[] array2 = category.split("\\.");
            category2=array2[0];
            for(int k=1;k<array2.length;k++){
                category2= new StringBuilder().append(category2).append(".").append(array2[k].substring(0, 1).toUpperCase()).append(array2[k].substring(1)).toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return category2;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView docImage;
        TextView docName, docDegree, docSpecialization, docDistance;
        EditText docNumber;

        private MyViewHolder(@NonNull View itemView) {
            super(itemView);
            docImage = itemView.findViewById(R.id.doctorImage);
            docName = itemView.findViewById(R.id.docName);
            docNumber = itemView.findViewById(R.id.docNumber);
            docDegree = itemView.findViewById(R.id.docDegree);
            docSpecialization = itemView.findViewById(R.id.docSpecialization);
            docDistance = itemView.findViewById(R.id.docDistance);
        }

        public void bind(final String item, final clickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClicked(item);
                }
            });
        }
    }
}
