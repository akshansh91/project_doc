package com.example.aksha.project_doc.utils;

public class ServerConfig {

    private String serverName = "http://akshanshmishra619.000webhostapp.com/";

    public String getDoctorDetails() {
        return serverName + "API/getDoctorDetails.php";
    }

    public String checkIfThatCategoryExists() {
        return serverName + "API/checkIfThatCategoryExists.php";
    }

    public String getDoctorDetailsComplete() {
        return serverName + "API/getDoctorDetailsComplete.php";
    }

    public String getDocListUsingHospital() {
        return serverName + "API/getDocListUsingHospital.php";
    }

    public String getDoctorDetailsUsingID() {
        return serverName + "API/getDoctorDetailsUsingID.php";
    }

    public String search() {
        return serverName + "API/searchData.php";
    }
}
