package com.example.aksha.project_doc;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.aksha.project_doc.Database.DatabaseHandler;
import com.example.aksha.project_doc.model.WishItem;
import com.example.aksha.project_doc.utils.ServerConfig;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class viewDoctorProfile extends AppCompatActivity implements DayCardRecyclerViewAdapter.productItemclickListener {

    String docID, TAG = "viewDoctorProfile", responseString, Category = "Doctor", TAG_ = "CLASS";
    ProgressDialog progressDialog;

    OkHttpClient client;
    HttpUrl.Builder builder;

    Request request;
    Response response;
    RecyclerView dayRecyclerView;
    private DayCardRecyclerViewAdapter mDayCardRecyclerViewAdapter;
    private ArrayList<String> mTimeline = new ArrayList<>();
    String timingM, timingE;
    private TextView proDocName, proDocAddress, proDocPhone, proDocCategory, proDocNote, proDocDegree;
    Button proCallDoc;
    AlertDialog alertDialog;
    String[] filterOptions;
    String filterSelected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.doc_profile1);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        docID = intent.getStringExtra("id");
        Log.d(TAG, "docID: " + docID);
        Log.d(TAG_, "viewDoctorProfile");

        progressDialog = new ProgressDialog(viewDoctorProfile.this);
        progressDialog.setTitle("Loading");
        progressDialog.setMessage("Please Wait!!");
        progressDialog.setIndeterminate(false);
        progressDialog.setCancelable(false);

        proDocName = findViewById(R.id.prodocname);
        proDocPhone = findViewById(R.id.prodocphone);
        proDocAddress = findViewById(R.id.prodocaddress);
        proDocCategory = findViewById(R.id.prodoccategory);
        proDocDegree = findViewById(R.id.prodocspeciality);
        proDocNote = findViewById(R.id.prodocnote);
        proCallDoc = findViewById(R.id.prodoccall);

        getDocDetails docDetails = new getDocDetails();
        docDetails.execute(docID);
        //initRecyclerView();
        proCallDoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (filterOptions.length != 1) {
                    loadFilterDialogBox();
                } else {
                    Toast.makeText(viewDoctorProfile.this, proDocPhone.getText(), Toast.LENGTH_SHORT).show();
                    Intent intent1 = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", String.valueOf(proDocPhone.getText()), null));
                    startActivity(intent1);
                }
            }
        });
    }

    public void loadFilterDialogBox() {

        //For the Dialog . . .
        try {
            LayoutInflater inflater = LayoutInflater.from(this);
            @SuppressLint("InflateParams") final View dialogView = inflater.inflate(R.layout.call_filter, null);
            alertDialog = new AlertDialog.Builder(this).create();
            alertDialog.setView(dialogView);
            alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            Window window = alertDialog.getWindow();
            WindowManager.LayoutParams lp = alertDialog.getWindow().getAttributes();
            lp.dimAmount = .7f;
            alertDialog.getWindow().setAttributes(lp);
            alertDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);

            if (window != null) {
                window.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            }
            if (window != null) {
                window.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            }

            RadioButton r1 = dialogView.findViewById(R.id.num1);
            RadioButton r2 = dialogView.findViewById(R.id.num2);

            r1.setText(filterOptions[0]);
            r2.setText(filterOptions[1]);

            RadioGroup radioGroup = dialogView.findViewById(R.id.sortOptionRadioGroup);
            radioGroup.clearCheck();

            radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @SuppressLint("ResourceType")
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    RadioButton rb = group.findViewById(checkedId);
                    if (null != rb && checkedId > -1) {
                        filterSelected = rb.getText().toString().toLowerCase();
                    }
                    Intent intent1 = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", filterSelected, null));
                    alertDialog.dismiss();
                    startActivity(intent1);
                }
            });

            dialogView.findViewById(R.id.cancelDialog).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                }
            });

        } catch (Exception e) {
            Log.d(TAG, String.valueOf(e));
        }
        alertDialog.show();
    }


    private void initRecyclerView() {

        //setup recycle view for showing day and timing
        dayRecyclerView = findViewById(R.id.dayrecycleview);

        if (mDayCardRecyclerViewAdapter == null) {
            mDayCardRecyclerViewAdapter = new DayCardRecyclerViewAdapter(this, timingM, timingE, mTimeline, this);
        }
        final LinearLayoutManager gridLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);

        /*SnapHelper snapHelper = new PagerSnapHelper();
        recyclerView.setLayoutManager(layoutManager);
        snapHelper.attachToRecyclerView(mRecyclerView);*/

        dayRecyclerView.setLayoutManager(gridLayoutManager);
        dayRecyclerView.setAdapter(mDayCardRecyclerViewAdapter);


    }

    /**
     * Click listener when card is clicked
     */
    @Override
    public void cardItemClicked(View v, int pos) {

    }

    @SuppressLint("StaticFieldLeak")
    class getDocDetails extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            String id = strings[0];

            client = new OkHttpClient.Builder().connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS).build();

            builder = Objects.requireNonNull(HttpUrl.parse(new ServerConfig().getDoctorDetailsUsingID())).newBuilder();
            builder.addQueryParameter("id", id);

            String url = builder.build().toString();

            request = new Request.Builder().url(url).build();

            try {
                response = client.newCall(request).execute();
            } catch (Exception e) {
                Log.d(TAG, "inside getDoctorsDetails doInBackground() exe_1: " + e);
            }

            if (response.body() != null) {
                try {
                    responseString = response.body().string();
                } catch (Exception e) {
                    Log.d(TAG, "inside getDoctorsDetails doInBackground() exe_2: " + e);
                }
            }
            Log.d(TAG, "inside getDoctorsDetails doInBackground() responseString: " + responseString);
            return responseString;
        }

        @Override
        protected void onPostExecute(String s) {
            progressDialog.dismiss();
            if (!s.equals("error")) {
                Log.d(TAG, "inside getDoctorsDetails onPostExecute() going to process response");
                processResponse(s);
            } else {
                Toast.makeText(viewDoctorProfile.this, "SERVER ERROR", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void processResponse(String s) {
        //adding data in different array lists ...
        String chunkOfData[] = s.split("___");

        int counter = 1;
        int size = chunkOfData.length;

        while (counter < size) {

            String blockOfData = chunkOfData[counter];
            String[] array = blockOfData.trim().split("\\*", -1);

            //name,degree,category,phone,address,timingM,timingE,m,tu,wed,th,fr,sat,su,note

            String name = array[0];
            Log.d("API_Call", "name: " + name);
            proDocName.setText(name.replace(" ", "  "));

            String degree = array[1];
            Log.d("API_Call", "degree: " + degree);
            proDocDegree.setText(degree.substring(0, 1).toUpperCase() + degree.substring(1));

            String category = array[2];
            Log.d("API_Call", "category: " + category);
            proDocCategory.setText(properCapitalise(category));

            String phone = array[3];
            filterOptions = phone.trim().split(",");
            //Log.d("API_Call_P", "phone1: " + filterOptions[0] + " phone2: " + filterOptions[1]);
            proDocPhone.setText(phone.replace(",", " , "));

            String address = array[4];
            Log.d("API_Call", "address: " + address);
            proDocAddress.setText(properCapitalise(address));

            timingM = array[5];
            Log.d("API_Call", "timingM: " + timingM);

            timingE = array[6];
            Log.d("API_Call", "timingE: " + timingE);

            String m = array[7];
            mTimeline.add(0, m);
            Log.d("API_Call", "m: " + m);

            String tu = array[8];
            mTimeline.add(1, tu);
            Log.d("API_Call", "tu: " + tu);

            String wed = array[9];
            mTimeline.add(2, wed);
            Log.d("API_Call", "wed: " + wed);

            String th = array[10];
            mTimeline.add(3, th);
            Log.d("API_Call", "th: " + th);

            String fr = array[11];
            mTimeline.add(4, fr);
            Log.d("API_Call", "fr: " + fr);

            String sat = array[12];
            mTimeline.add(5, sat);
            Log.d("API_Call", "sat: " + sat);

            String sun = array[13];
            mTimeline.add(6, sun);
            Log.d("API_Call", "sun: " + sun);

            /*String fees = array[13];
            Log.d("API_Call", "fees: " + fees);*/

            String note = array[14];
            Log.d("API_Call", "note: " + note);
            if (!note.equals("") && note.length() > 0) {
                LinearLayout notetext = (LinearLayout) findViewById(R.id.prodocnotecontainer);
                notetext.setVisibility(View.VISIBLE);
                proDocNote.setText(properCapitalise(note));
            }
            counter++;
        }
        initRecyclerView();
        addItemToRecentlyView();
    }

    public String properCapitalise(String string) {

        String category = "";
        String[] array = string.split(" ");
        for (int k = 0; k < array.length; k++) {
            category = new StringBuilder().append(category).append(array[k].substring(0, 1).toUpperCase()).append(array[k].substring(1).toLowerCase()).append(" ").toString();
        }

        String category2 = "";
        try {
            String[] array2 = category.split("\\.");
            category2 = array2[0];
            for (int k = 1; k < array2.length; k++) {
                category2 = new StringBuilder().append(category2).append(".").append(array2[k].substring(0, 1).toUpperCase()).append(array2[k].substring(1)).toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return category2;
    }

    private void addItemToRecentlyView() {
        DatabaseHandler recentdb = new DatabaseHandler(this);
        //check if already exist
        if (!recentdb.getAllRecent().isEmpty()) {
            for (WishItem wish : recentdb.getAllRecent()) {
                if (wish.getProductId().equals(docID)) {
                    Log.d("MainActivity", "Item already exists start delete : success " + recentdb.getAllRecent().size());
                    recentdb.deleteRecentItem(wish);
                    Log.d("MainActivity", "Item already exists so deleted : success " + recentdb.getAllRecent().size());
                }
            }
        }
        String currentdate = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        recentdb.addRecent(new WishItem(Category, docID, currentdate));
        Log.d("MainActivity", "addItem ToRecentlyView : success ");
    }

    public void addItemToWishList() {
        DatabaseHandler recentdb = new DatabaseHandler(this);
        Log.d("MainActivity", "addItem TO WISH START 1 : success ");
        //check if already exist
        boolean exist = checkIfAlreadyFavourite();
        if (exist)
            makeSnackBarMessage("Item already exists! ");

        if (!exist) {
            String currentdate = new SimpleDateFormat("yyyyMMdd_HHmmss",
                    Locale.getDefault()).format(new Date());
            recentdb.addWish(new WishItem(Category, docID, currentdate));
            Log.d("MainActivity", "addItem To WishList 2 : success ");
            makeSnackBarMessage("Item added to Favourite! ");
        }
    }

    public boolean checkIfAlreadyFavourite() {
        DatabaseHandler recentdb = new DatabaseHandler(this);
        Log.d("MainActivity", "Checking item in wishlist if already exist : starts ");
        boolean exist = false;
        //check if already exist
        if (!recentdb.getAllWish().isEmpty()) {
            for (WishItem wish : recentdb.getAllWish()) {
                if (wish.getProductId().equals(docID)) {
                    exist = true;
                    //makeSnackBarMessage("Item already exists! ");
                    Log.d("MainActivity", "Item already exists  : success " + recentdb.getAllWish().size());
                }
            }
        }
        return exist;
    }

    private void makeSnackBarMessage(String message) {
        View mLinearLayout = findViewById(R.id.docprofilecontainer);
        Snackbar.make(mLinearLayout, message, Snackbar.LENGTH_SHORT).show();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if (checkIfAlreadyFavourite()) {
            getMenuInflater().inflate(R.menu.favbtn, menu);
            menu.getItem(0).setIcon(getResources().getDrawable(R.drawable.stared));
        } else
            getMenuInflater().inflate(R.menu.favbtn, menu);

        // getMenuInflater().inflate(R.layout.custom_actionbar,menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            Log.v("MainActivity", "Backpress button is clicked ");
            onBackPressed();
            return true;
        } else if (item.getItemId() == R.id.favouritebtn) {
            addItemToWishList();
            item.setIcon(getResources().getDrawable(R.drawable.stared));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
