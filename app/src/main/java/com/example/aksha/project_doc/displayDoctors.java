package com.example.aksha.project_doc;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.aksha.project_doc.utils.ServerConfig;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class displayDoctors extends AppCompatActivity {

    String category, responseString, TAG = "displayDoctors";
    Toolbar toolbar;
    RecyclerView viewDoctorList;

    OkHttpClient client;
    HttpUrl.Builder builder;

    Request request;
    Response response;

    ProgressDialog progressDialog;

    ArrayList<String> list;
    ArrayList<ArrayList<String>> finalList;

    String TAG_ = "CLASS";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_doctors);

        Log.d(TAG_, "displayDoctors");

        Intent intent = getIntent();
        category = intent.getStringExtra("category");
        Log.d(TAG, "category: " + category);

        toolbar = findViewById(R.id.toolbarDisplayDoctors);
        toolbar.setTitle(category.substring(0, 1).toUpperCase() + category.substring(1));

        try {
            //toolbar.setNavigationIcon(R.id.);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
            // getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        finalList = new ArrayList<>();

        viewDoctorList = findViewById(R.id.doctorList);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(displayDoctors.this, LinearLayoutManager.VERTICAL, false);
        viewDoctorList.setLayoutManager(linearLayoutManager);

        progressDialog = new ProgressDialog(displayDoctors.this);
        progressDialog.setTitle("Loading");
        progressDialog.setMessage("Please Wait!!");
        progressDialog.setIndeterminate(false);
        progressDialog.setCancelable(false);

        checkIfThatCategoryExists categoryExists = new checkIfThatCategoryExists();
        categoryExists.execute(category);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        /*Intent intent = new Intent(displayDoctors.this, MainActivity.class);
        startActivity(intent);
        finish();*/
    }

    @SuppressLint("StaticFieldLeak")
    class getDoctorsDetails extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {

            Log.d(TAG, "inside getDoctorsDetails doInBackground()");

            String category = strings[0];

            client = new OkHttpClient.Builder().connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS).build();

            builder = Objects.requireNonNull(HttpUrl.parse(new ServerConfig().getDoctorDetails())).newBuilder();
            builder.addQueryParameter("category", category);

            String url = builder.build().toString();

            request = new Request.Builder().url(url).build();

            try {
                response = client.newCall(request).execute();
            } catch (Exception e) {
                Log.d(TAG, "inside getDoctorsDetails doInBackground() exe_1: " + e);
            }

            if (response.body() != null) {
                try {
                    responseString = response.body().string();
                } catch (Exception e) {
                    Log.d(TAG, "inside getDoctorsDetails doInBackground() exe_2: " + e);
                }
            }
            Log.d(TAG, "inside getDoctorsDetails doInBackground() responseString: " + responseString);
            return responseString;
        }

        @Override
        protected void onPostExecute(String s) {
            if (!s.equals("error")) {
                progressDialog.dismiss();
                Log.d(TAG, "inside getDoctorsDetails onPostExecute() going to process response");
                processResponse(s);
            } else {
                Toast.makeText(displayDoctors.this, "SERVER ERROR", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    class checkIfThatCategoryExists extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {

            Log.d(TAG, "inside checkIfThatCategoryExists doInBackground()");

            String category = strings[0];

            client = new OkHttpClient.Builder().connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS).build();

            builder = Objects.requireNonNull(HttpUrl.parse(new ServerConfig().checkIfThatCategoryExists())).newBuilder();
            builder.addQueryParameter("category", category);

            String url = builder.build().toString();

            request = new Request.Builder().url(url).build();

            try {
                response = client.newCall(request).execute();
            } catch (IOException e) {
                Log.d(TAG, "inside checkIfThatCategoryExists doInBackground() exe_1: " + e);
            }

            if (response.body() != null) {
                try {
                    responseString = response.body().string();
                } catch (IOException e) {
                    Log.d(TAG, "responseString: " + responseString);
                    Log.d(TAG, "inside checkIfThatCategoryExists doInBackground() exe_2: " + e);
                }
            }
            return responseString;
        }

        @Override
        protected void onPostExecute(String s) {
            Log.d(TAG, "inside checkIfThatCategoryExists onPostExecute()");
            Log.d(TAG, "inside checkIfThatCategoryExists string: " + s);

            if (!s.equals("yes")) {
                progressDialog.dismiss();

                Intent intent = new Intent(displayDoctors.this, DocCategoryClass.class);
                startActivity(intent);

                Toast.makeText(displayDoctors.this, "No " + category + " exists!", Toast.LENGTH_SHORT).show();
            } else {
                Log.d(TAG, "going to getList()");
                getList(category);
            }
        }
    }

    private void getList(String category) {
        //get list from the server. . .
        Log.d(TAG, "inside getList()");
        getDoctorsDetails doctorsDetails = new getDoctorsDetails();
        doctorsDetails.execute(category);
    }

    private void processResponse(String s) {
        //adding data in different array lists ...
        String chunkOfData[] = s.split("___");

        int counter = 1;
        int size = chunkOfData.length;

        while (counter < size) {
            list = new ArrayList<>();

            String blockOfData = chunkOfData[counter];
            String[] array = blockOfData.trim().split("\\*", -1);

            //name,degree,phone,address,timingM,timingE,m,tu,wed,th,fr,sat,su,note

            String name = array[0];
            list.add(name);
            Log.d("API_Call", "name: " + name);

            String degree = array[1];
            list.add(degree);
            Log.d("API_Call", "degree: " + degree);

            String phone = array[2];
            list.add(phone);
            Log.d("API_Call", "phone: " + phone);

            String address = array[3];
            list.add(address);
            Log.d("API_Call", "address: " + address);

            String timingM = array[4];
            list.add(timingM);
            Log.d("API_Call", "timingM: " + timingM);

            String timingE = array[5];
            list.add(timingE);
            Log.d("API_Call", "timingE: " + timingE);

            String m = array[6];
            list.add(m);
            Log.d("API_Call", "m: " + m);

            String tu = array[7];
            list.add(tu);
            Log.d("API_Call", "tu: " + tu);

            String wed = array[8];
            list.add(wed);
            Log.d("API_Call", "wed: " + wed);

            String th = array[9];
            list.add(th);
            Log.d("API_Call", "th: " + th);

            String fr = array[10];
            list.add(fr);
            Log.d("API_Call", "fr: " + fr);

            String sat = array[11];
            list.add(sat);
            Log.d("API_Call", "sat: " + sat);

            String sun = array[12];
            list.add(sun);
            Log.d("API_Call", "sun: " + sun);

            /*String fees = array[13];
            list.add(fees);
            Log.d("API_Call", "fees: " + fees);*/

            String note = array[13];
            list.add(note);
            Log.d("API_Call", "note: " + note);

            String id = array[14];
            list.add(id);
            Log.d("API_Call", "id: " + id);

            Log.d("API_Call", "List" + counter + ":" + list);
            finalList.add(list);
            counter++;
        }

        Adapter_DocList adapterDocList = new Adapter_DocList(displayDoctors.this, finalList, new Adapter_DocList.clickListener() {
            @Override
            public void onItemClicked(String id) {
                Intent intent = new Intent(displayDoctors.this, viewDoctorProfile.class);
                intent.putExtra("id", id);
                Log.d("displayDoctors", "id :" + id);
                startActivity(intent);
            }
        });
        viewDoctorList.setAdapter(adapterDocList);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            Log.v("MainActivity", "Backpress button is clicked ");
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
