package com.example.aksha.project_doc;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class Adapter_Doc_Category extends RecyclerView.Adapter<Adapter_Doc_Category.MyViewHolder> {

    public interface clickListener {
        void onItemClicked(String image);
    }

    private Context myContext;
    private ArrayList<String> myName;
    private ArrayList<Integer> imageId;
    private LayoutInflater inflater;
    private clickListener myClickListener;

    Adapter_Doc_Category(Context context, ArrayList<String> name, ArrayList<Integer> imageId, clickListener clickListener) {
        inflater = LayoutInflater.from(context);
        myContext = context;
        myName = name;
        this.imageId = imageId;
        myClickListener = clickListener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        Log.d("package", myContext.getPackageName());
        View view = inflater.inflate(R.layout.card_design, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        String title = myName.get(i);
        int imgID = imageId.get(i);

        String titleToShow = title.substring(0, 1).toUpperCase() + title.substring(1);

        myViewHolder.bind(title, myClickListener);
        myViewHolder.title.setText(titleToShow);
        myViewHolder.imageView.setImageResource(imgID);
    }

    @Override
    public int getItemCount() {
        return myName.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView title;

        MyViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.pic);
            title = itemView.findViewById(R.id.title);
        }

        public void bind(final String item, final clickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClicked(item);
                }
            });
        }
    }
}
