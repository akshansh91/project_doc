package com.example.aksha.project_doc;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by shiva on 28-12-2018.
 */

class DayCardRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "ProductRecyclerViewAdapter";
    private productItemclickListener listener;

    private ArrayList<String> mtimeline;
    private String[] days = {"MON", "TUE", "WED", "THR", "FRI", "SAT", "SUN"};
    private List<String> mdaylist;
    // private IMainActivity mIMainActivity;
    private Context mContext;
    private int mSelectedNoteIndex;
    String mtime, etime;
    String TAG_ = "CLASS";

    public DayCardRecyclerViewAdapter(Context context, String mtime, String etime, ArrayList<String> mtimeline, productItemclickListener listener) {
        this.mtimeline = mtimeline;
        mContext = context;
        this.listener = listener;
        mdaylist = Arrays.asList(days);
        this.etime = etime;
        this.mtime = mtime;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder;
        View view = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.day_card, parent, false);

        holder = new ViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof ViewHolder) {
            Log.d("MainActivity", "Doc card view :" + position);
            ((ViewHolder) holder).daytextview.setText(mdaylist.get(position));
            if (mtimeline.get(position).equals("1")) {
                ((ViewHolder) holder).time1textview.setText(mtime);
                ((ViewHolder) holder).time2textview.setText(etime);

            } else {
                ((ViewHolder) holder).time1textview.setText(" ");
                ((ViewHolder) holder).time2textview.setText(" ");
            }
          /*  Glide.with(mContext)
                    .load(R.drawable.gradient_backgroundpink)
                    .into( ((ViewHolder)holder).dayimage);*/
            Log.d("MainActivity", "Doc card view complete");

        }
    }

    @Override
    public int getItemCount() {
        return 7;//mtimeline.size()
    }


    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        // mIMainActivity = (IMainActivity) mContext;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public interface productItemclickListener {
        public void cardItemClicked(View v, int pos);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView time1textview, time2textview, daytextview;
        ImageView dayimage;

        public ViewHolder(final View itemView) {
            super(itemView);
            time1textview = itemView.findViewById(R.id.time1_textview2);
            time2textview = itemView.findViewById(R.id.time2_textview);
            dayimage = itemView.findViewById(R.id.card_image);
            daytextview = itemView.findViewById(R.id.card_day);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = getAdapterPosition();
                    // check if item still exists
                    if (pos != RecyclerView.NO_POSITION) {
                       /* Product clickedDataItem = mProducts.get(pos);

                        listener.productItemClicked(v,pos,clickedDataItem);*/

                    }
                }
            });
        }
    }

}



