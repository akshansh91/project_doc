package com.example.aksha.project_doc;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class Adapter_HospitalList extends RecyclerView.Adapter<Adapter_HospitalList.MyViewHolder> {

    private Context context;
    private ArrayList<String> hosName;
    private ArrayList<String> hosAddress;
    private LayoutInflater inflater;
    public hospitalClickListener clickListener;

    public Adapter_HospitalList(Context context, ArrayList<String> hosName, ArrayList hosAddress, hospitalClickListener clickListener) {
        this.context = context;
        this.hosName = hosName;
        this.hosAddress = hosAddress;
        this.inflater = LayoutInflater.from(context);
        this.clickListener = clickListener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        Log.d("Context", context.getPackageName());
        View view = inflater.inflate(R.layout.hospital_list_card, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        if (myViewHolder instanceof MyViewHolder) {

            ((MyViewHolder) myViewHolder).name.setText(hosName.get(i).toUpperCase());
            ((MyViewHolder) myViewHolder).address.setText(hosAddress.get(i));
           /* Glide.with(mContext)
                    .load(mProducts.get(position).getImgurl1())
                    .into( ((MyViewHolder)myViewHolder).view);*/

        }
    }

    public interface hospitalClickListener {
        public void hospitalItemClicked(View v, int pos);
    }

    @Override
    public int getItemCount() {

        if (hosName.size() == hosAddress.size())
            return hosName.size();
        else
            return 0;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView view;
        TextView name, address;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            view = itemView.findViewById(R.id.pic);
            name = itemView.findViewById(R.id.hospitalName);
            address = itemView.findViewById(R.id.hospitalAddress);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = getAdapterPosition();
                    // check if item still exists
                    if (pos != RecyclerView.NO_POSITION) {
                        clickListener.hospitalItemClicked(v, pos);
                    }
                }
            });
        }
    }
}
