package com.example.aksha.project_doc; /**
 * Created by shiva on 13-01-2019.
 */

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.widget.FrameLayout;

public class BezierCurveBulgeLayout extends FrameLayout {

    AttributeSet attrs;
    int defStyleAttr = 0;
    int defStyleRes = 0;

    private int curveWidth;
    private int flatWidth;
    private int flatHeight;
    private int bulgeColor;
    private BulgeType bulgeType;


    private Path path = new Path();
    private Paint paint = new Paint();

    private Point startCurveStartPoint = new Point();
    private Point startCurveEndPoint = new Point();
    private Point startCurveFirstControlPoint = new Point();
    private Point startCurveSecondControlPoint = new Point();

    private Point endCurveStartPoint = new Point();
    private Point endCurveEndPoint = new Point();
    private Point endCurveFirstControlPoint = new Point();
    private Point endCurveSecondControlPoint = new Point();

    public BezierCurveBulgeLayout(@NonNull Context context) {
        super(context);
        init();
    }


    /*
     <com.example.shiva.mayankstore.BezierCurveBulgeLayout
        android:layout_width="match_parent"
        android:layout_height="70dp"
        android:layout_gravity="bottom"
        android:background="@color/colorPrimary"
        android:visibility="gone"
        app:bulgeColor="@color/colorPrimary"
        app:bulgeType="bulge"
        app:curveWidth="55dp"
        app:flatHeight="25dp"
        tools:layout_editor_absoluteX="0dp"
        tools:layout_editor_absoluteY="491dp">

        <ImageView
            android:id="@+id/imageView2"
            android:layout_width="50dp"
            android:layout_height="50dp"
            android:layout_gravity="center"
            app:srcCompat="@android:drawable/ic_menu_add" />
    </com.example.shiva.mayankstore.BezierCurveBulgeLayout>
     */


    public BezierCurveBulgeLayout(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.attrs = attrs;

        init();
    }

    public BezierCurveBulgeLayout(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.attrs = attrs;
        this.defStyleAttr = defStyleAttr;
        init();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public BezierCurveBulgeLayout(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.attrs = attrs;
        this.defStyleAttr = defStyleAttr;
        this.defStyleRes = defStyleRes;
        init();
    }


    private void init() {

        TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.BezierCurveBulgeLayout, defStyleAttr, defStyleRes);

        curveWidth = a.getDimensionPixelSize(R.styleable.BezierCurveBulgeLayout_curveWidth, 0);
        flatWidth = a.getDimensionPixelSize(R.styleable.BezierCurveBulgeLayout_flatWidth, 0);
        flatHeight = a.getDimensionPixelSize(R.styleable.BezierCurveBulgeLayout_flatHeight, 0);
        bulgeType = BulgeType.values()[a.getInt(R.styleable.BezierCurveBulgeLayout_bulgeType, BulgeType.BULGE.ordinal())];
        bulgeColor = a.getColor(R.styleable.BezierCurveBulgeLayout_bulgeColor, Color.WHITE);

        a.recycle();

        paint.setStyle(Paint.Style.FILL_AND_STROKE);
        paint.setColor(bulgeColor);
        setBackgroundColor(Color.TRANSPARENT);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        int viewWidth = getWidth();
        int viewHeight = getHeight();

        int baseY = 0;
        int flatY = 0;

        if (bulgeType == BulgeType.BULGE) baseY = flatHeight;
        else flatY = flatHeight;


        startCurveStartPoint.set(viewWidth / 2 - flatWidth / 2 - curveWidth * 7 / 6, baseY);
        startCurveEndPoint.set(viewWidth / 2 - flatWidth / 2, flatY);


        endCurveStartPoint.set(viewWidth / 2 + flatWidth / 2, flatY);
        endCurveEndPoint.set(viewWidth / 2 + flatWidth / 2 + curveWidth * 7 / 6, baseY);


        startCurveFirstControlPoint.set(startCurveStartPoint.x + curveWidth * 5 / 8, startCurveStartPoint.y);
        startCurveSecondControlPoint.set(startCurveEndPoint.x - curveWidth / 2, startCurveEndPoint.y);

        endCurveFirstControlPoint.set(endCurveStartPoint.x + curveWidth / 2, endCurveStartPoint.y);
        endCurveSecondControlPoint.set(endCurveEndPoint.x - curveWidth * 5 / 8, endCurveEndPoint.y);

        path.reset();
        path.moveTo(0f, baseY);
        path.lineTo((float) startCurveStartPoint.x, (float) startCurveStartPoint.y);

        path.cubicTo(
                (float) startCurveFirstControlPoint.x, (float) startCurveFirstControlPoint.y,
                (float) startCurveSecondControlPoint.x, (float) startCurveSecondControlPoint.y,
                (float) startCurveEndPoint.x, (float) startCurveEndPoint.y
        );

        path.lineTo((float) endCurveStartPoint.x, (float) endCurveStartPoint.y);

        path.cubicTo(
                (float) endCurveFirstControlPoint.x, (float) endCurveFirstControlPoint.y,
                (float) endCurveSecondControlPoint.x, (float) endCurveSecondControlPoint.y,
                (float) endCurveEndPoint.x, (float) endCurveEndPoint.y
        );

        path.lineTo((float) viewWidth, (float) baseY);
        path.lineTo((float) viewWidth, (float) viewHeight);
        path.lineTo(0f, (float) viewHeight);
        path.close();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawPath(path, paint);
    }

    public enum BulgeType {
        BULGE,
        NOTCH
    }

}
