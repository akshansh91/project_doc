package com.example.aksha.project_doc;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.aksha.project_doc.utils.ServerConfig;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

@SuppressWarnings("ALL")
public class lookDoctorsAroundClass extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private static final int PLAY_SERVICE_RESOLUTION_REQUEST = 7172;
    boolean mRequestingLocationUpdates = false;

    //The GOOGLE thing
    LocationRequest locationRequest;
    GoogleApiClient googleApiClient;
    Location location, mLastLocation;
    LocationManager manager;
    String docName, docID;

    int updateInterval = 5000;//in sec...
    int fastestInterval = 3000;//in sec...
    int displacement = 10; //in metres...
    //Done with google stuff ...

    RecyclerView doctorAroundRecyclerView, doctorCategoryRecyclerView;
    Adapter_DocAround adapterDocAround;
    Adapter_Doc_Category_RecyclerView adapter_doc_category_recyclerView;

    OkHttpClient client;
    HttpUrl.Builder builder;

    Request request;
    Response response;

    ProgressDialog progressDialog;

    String responseString, TAG = "lookDoctorsAround", docCategory = "none";

    double myLatitude, myLongitude;

    ArrayList<String> list;
    ArrayList<ArrayList<String>> finalList;
    ArrayList<String> categoryName;

    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_look_doctors_around_class);

        manager = (LocationManager) this.getSystemService(LOCATION_SERVICE);
        boolean isEnabledGPS = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean isEnabledNetwork = manager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        if (!isEnabledGPS && !isEnabledNetwork) {
            showSettingAlert();
        }

        if (checkPlayServices()) {
            buildGoogleApiClient();
            createLocationRequest();
        }

        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //setting up the category names
        categoryName = new ArrayList<>();
        categoryName.add("haematologist");
        categoryName.add("medicine");
        categoryName.add("neuro physician");
        categoryName.add("m.d. physician");
        categoryName.add("microbiologist");
        categoryName.add("maternity and child health");
        categoryName.add("neuro surgeon");
        categoryName.add("oncology cancer");
        categoryName.add("nephrologist");
        categoryName.add("neurologist");
        categoryName.add("opthamology");
        categoryName.add("orthopaedic");
        categoryName.add("obct");
        categoryName.add("occupational therapist");
        categoryName.add("paediatrician");
        categoryName.add("dentist");
        categoryName.add("critical care specialist");
        categoryName.add("cardio thoracic surgeon");
        categoryName.add("cardiology");
        categoryName.add("consultant physician");
        categoryName.add("audiologist");
        categoryName.add("ayurvedic");
        categoryName.add("occupational theraoist");

        doctorCategoryRecyclerView = findViewById(R.id.docCategoryRecyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(lookDoctorsAroundClass.this, LinearLayoutManager.HORIZONTAL, false);
        doctorCategoryRecyclerView.setLayoutManager(linearLayoutManager);

        adapter_doc_category_recyclerView = new Adapter_Doc_Category_RecyclerView(lookDoctorsAroundClass.this,
                categoryName, new Adapter_Doc_Category_RecyclerView.clickListener() {
            @Override
            public void onItemClicked(String category) {
                docCategory = category;
                checkIfThatCategoryExists checkIfThatCategoryExists = new checkIfThatCategoryExists();
                checkIfThatCategoryExists.execute(docCategory);
            }
        });

        doctorCategoryRecyclerView.setAdapter(adapter_doc_category_recyclerView);

        finalList = new ArrayList<>();

        progressDialog = new ProgressDialog(lookDoctorsAroundClass.this);
        progressDialog.setTitle("Loading");
        progressDialog.setMessage("Please Wait!!");
        progressDialog.setIndeterminate(false);
        progressDialog.setCancelable(false);

        getData getData = new getData();
        getData.execute(docCategory);
        //set the category ...

        doctorAroundRecyclerView = findViewById(R.id.doctorsAroundRecyclerView);
        LinearLayoutManager manager = new LinearLayoutManager(lookDoctorsAroundClass.this, LinearLayoutManager.VERTICAL, false);
        doctorAroundRecyclerView.setLayoutManager(manager);
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this, PLAY_SERVICE_RESOLUTION_REQUEST).show();
            } else {
                Toast.makeText(this, "Not Supported on this device", Toast.LENGTH_SHORT).show();
                finish();
            }
            return false;
        }
        return true;
    }

    private void buildGoogleApiClient() {
        googleApiClient = new GoogleApiClient.Builder(this).
                addConnectionCallbacks(this).
                addOnConnectionFailedListener(this).
                addApi(LocationServices.API).build();

        googleApiClient.connect();
        createLocationRequest();
    }

    private void createLocationRequest() {
        locationRequest = new LocationRequest();
        locationRequest.setInterval(updateInterval);
        locationRequest.setFastestInterval(fastestInterval);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setSmallestDisplacement(displacement);
    }

    private void displayLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        if (location != null) {
            myLatitude = location.getLatitude();
            myLongitude = location.getLongitude();
            Toast.makeText(this, "Latitude: " + myLatitude + " Longitude: " + myLongitude, Toast.LENGTH_SHORT).show();
        }
    }

    private void showSettingAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        alertDialog.setTitle("Start GPS");

        alertDialog.setMessage("GPS not enabled!!!");

        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        });

        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        alertDialog.show();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        displayLocation();
        if (mRequestingLocationUpdates) {
            startLocationUpdates();
        }
    }

    private void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        displayLocation();
    }

    @SuppressLint("StaticFieldLeak")
    class getData extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {

            String category = strings[0];
            Log.d(TAG, "doInBackground() category: " + category);

            client = new OkHttpClient.Builder().connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS).build();

            builder = Objects.requireNonNull(HttpUrl.parse(new ServerConfig().getDoctorDetailsComplete())).newBuilder();
            builder.addQueryParameter("category", category);

            String url = builder.build().toString();

            request = new Request.Builder().url(url).build();

            try {
                response = client.newCall(request).execute();
            } catch (Exception e) {
                Log.d(TAG, "inside lookDoctorsAround doInBackground() exe_1: " + e);
            }

            if (response.body() != null) {
                try {
                    responseString = response.body().string();
                } catch (Exception e) {
                    Log.d(TAG, "inside lookDoctorsAround doInBackground() exe_2: " + e);
                }
            }
            Log.d(TAG, "inside lookDoctorsAround doInBackground() responseString: " + responseString);
            return responseString;
        }

        @Override
        protected void onPostExecute(String s) {
            if (!s.equals("error")) {
                progressDialog.dismiss();
                Log.d(TAG, "inside lookDoctorsAround onPostExecute() going to process response");
                processResponse(s);
            } else {
                Toast.makeText(lookDoctorsAroundClass.this, "SERVER ERROR", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    class checkIfThatCategoryExists extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {

            Log.d(TAG, "inside checkIfThatCategoryExists doInBackground()");

            String category = strings[0];

            client = new OkHttpClient.Builder().connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS).build();

            builder = Objects.requireNonNull(HttpUrl.parse(new ServerConfig().checkIfThatCategoryExists())).newBuilder();
            builder.addQueryParameter("category", category);

            String url = builder.build().toString();

            request = new Request.Builder().url(url).build();

            try {
                response = client.newCall(request).execute();
            } catch (IOException e) {
                Log.d(TAG, "inside checkIfThatCategoryExists doInBackground() exe_1: " + e);
            }

            if (response.body() != null) {
                try {
                    responseString = response.body().string();
                } catch (IOException e) {
                    Log.d(TAG, "responseString: " + responseString);
                    Log.d(TAG, "inside checkIfThatCategoryExists doInBackground() exe_2: " + e);
                }
            }
            return responseString;
        }

        @Override
        protected void onPostExecute(String s) {
            Log.d(TAG, "inside checkIfThatCategoryExists onPostExecute()");
            Log.d(TAG, "inside checkIfThatCategoryExists string: " + s);

            if (!s.equals("yes")) {
                progressDialog.dismiss();
                Toast.makeText(lookDoctorsAroundClass.this, docCategory.toUpperCase() + " doesn't exists", Toast.LENGTH_SHORT).show();
                getData getData = new getData();
                getData.execute("none");
            } else {
                getData getData = new getData();
                getData.execute(docCategory);
            }
        }
    }

    private void processResponse(String s) {
        //adding data in different array lists ...
        String chunkOfData[] = s.split("___");

        finalList.clear();

        int counter = 1;
        int size = chunkOfData.length;

        while (counter < size) {
            list = new ArrayList<>();

            String blockOfData = chunkOfData[counter];
            String[] array = blockOfData.trim().split("\\*", -1);

            //name,degree,phone,address,timingM,timingE,m,tu,wed,th,fr,sat,su,note,lat,long,category,id ...

            String name = array[0];
            docName = name;
            list.add(name);
            Log.d("API_Call", "name: " + name);

            String degree = array[1];
            list.add(degree);
            Log.d("API_Call", "degree: " + degree);

            String phone = array[2];
            list.add(phone);
            Log.d("API_Call", "phone: " + phone);

            String address = array[3];
            list.add(address);
            Log.d("API_Call", "address: " + address);

            String timingM = array[4];
            list.add(timingM);
            Log.d("API_Call", "timingM: " + timingM);

            String timingE = array[5];
            list.add(timingE);
            Log.d("API_Call", "timingE: " + timingE);

            String m = array[6];
            list.add(m);
            Log.d("API_Call", "m: " + m);

            String tu = array[7];
            list.add(tu);
            Log.d("API_Call", "tu: " + tu);

            String wed = array[8];
            list.add(wed);
            Log.d("API_Call", "wed: " + wed);

            String th = array[9];
            list.add(th);
            Log.d("API_Call", "th: " + th);

            String fr = array[10];
            list.add(fr);
            Log.d("API_Call", "fr: " + fr);

            String sat = array[11];
            list.add(sat);
            Log.d("API_Call", "sat: " + sat);

            String sun = array[12];
            list.add(sun);
            Log.d("API_Call", "sun: " + sun);

            /*String fees = array[13];
            list.add(fees);
            Log.d("API_Call", "fees: " + fees);*/

            String note = array[13];
            list.add(note);
            Log.d("API_Call", "note: " + note);

            double latitudeDR = Double.parseDouble(array[14]);
            double longitudeDR = Double.parseDouble(array[15]);

            String category = array[16];
            list.add(category);
            Log.d("API_Call", "category: " + category);

            String id = array[17];
            docID = id;
            list.add(id);

            String distance = calculateDistance(myLatitude, myLongitude, latitudeDR, longitudeDR);

            list.add(distance);
            Log.d("API_Call", "distance: " + distance);

            Log.d("API_Call", "List" + counter + ":" + list);
            finalList.add(list);
            counter++;
        }
        adapterDocAround = new Adapter_DocAround(lookDoctorsAroundClass.this, finalList, new Adapter_DocAround.clickListener() {
            @Override
            public void onItemClicked(String id) {
                Intent intent = new Intent(lookDoctorsAroundClass.this, viewDoctorProfile.class);
                intent.putExtra("id", id);
                Log.d("lookDoctorAroundClass", "docID :" + id);
                startActivity(intent);
                //Toast.makeText(lookDoctorsAroundClass.this, image, Toast.LENGTH_SHORT).show();
            }
        });
        doctorAroundRecyclerView.setAdapter(adapterDocAround);
        adapterDocAround.notifyDataSetChanged();
    }

    private String calculateDistance(double myLatitude, double myLongitude, double latitudeDR, double longitudeDR) {
        Location start = new Location("start");
        start.setLatitude(myLatitude);
        start.setLongitude(myLongitude);

        Location end = new Location("end");
        end.setLatitude(latitudeDR);
        end.setLongitude(longitudeDR);

        double distance = start.distanceTo(end);
        Log.d("distance", String.valueOf(distance));
        String result = String.format("%.2f", distance);

        return result;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            Log.v("MainActivity", "Backpress button is clicked ");
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
