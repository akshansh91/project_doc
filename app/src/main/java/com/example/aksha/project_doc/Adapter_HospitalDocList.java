package com.example.aksha.project_doc;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class Adapter_HospitalDocList extends RecyclerView.Adapter<Adapter_HospitalDocList.MyViewHolder> {

    public interface clickListener {
        void onItemClicked(String image);
    }

    private Context context;
    private ArrayList<ArrayList<String>> data;
    private LayoutInflater inflater;
    /*private int mExpandedPosition = -1;
    private int pos;*/
    public clickListener myClickListener;

    public Adapter_HospitalDocList(Context context, ArrayList<ArrayList<String>> data, clickListener clickListener) {
        this.context = context;
        this.data = data;
        inflater = LayoutInflater.from(context);
        myClickListener = clickListener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = inflater.inflate(R.layout.hosp_doc_card, viewGroup, false);
        return new MyViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        //specifying the elements from the list to display in the list ...
        //according to a particular position ...

        //name,degree,phone,address,timingM,timingE,m,tu,wed,th,fr,sat,su,note

        //pos = myViewHolder.getAdapterPosition();

        /*final boolean isExpanded = i == mExpandedPosition;
        myViewHolder.subItem.setVisibility(isExpanded ? View.VISIBLE : View.GONE);
        myViewHolder.itemView.setActivated(isExpanded);

        myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mExpandedPosition = isExpanded ? -1 : pos;
                notifyDataSetChanged();
            }
        });*/

        String docName = data.get(i).get(0);
        String docDegree = data.get(i).get(1).replace(",","   |  ");
        final String docNumber = data.get(i).get(2);
        String docAddress = data.get(i).get(3);
        String timingM = data.get(i).get(4);
        String timingE = data.get(i).get(5);
        int m = Integer.parseInt(data.get(i).get(6));
        int tu = Integer.parseInt(data.get(i).get(7));
        int wed = Integer.parseInt(data.get(i).get(8));
        int th = Integer.parseInt(data.get(i).get(9));
        int fr = Integer.parseInt(data.get(i).get(10));
        int sat = Integer.parseInt(data.get(i).get(11));
        int sun = Integer.parseInt(data.get(i).get(12));
        String note = data.get(i).get(13);
        String id = data.get(i).get(14);
        String category="";

        myViewHolder.bind(id, myClickListener);

        myViewHolder.docDegree.setText(docDegree);

        //myViewHolder.docNumber.setText(docNumber);
        myViewHolder.docName.setText(docName);
        /*myViewHolder.docAddress.setText(docAddress);
        myViewHolder.timeMorning.setText(timingM);
        myViewHolder.timeEvening.setText(timingE);
        myViewHolder.note.setText(context.getString(R.string.note) + note);*/


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView docImage;
        TextView docName,docDegree;// docNumber, docDegree, timeMorning, timeEvening, call, fees;

        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            docImage = itemView.findViewById(R.id.hospdocpic);
            docName = itemView.findViewById(R.id.hospdocname);
            docDegree = itemView.findViewById(R.id.hosdocdegree);

        }

        public void bind(final String item, final clickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClicked(item);
                }
            });
        }

    }
}
