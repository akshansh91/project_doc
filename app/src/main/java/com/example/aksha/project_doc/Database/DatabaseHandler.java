package com.example.aksha.project_doc.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.aksha.project_doc.model.WishItem;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHandler extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "ProductManager";
    private static final String TABLE_RECENT = "RecentItemTable";
    private static final String TABLE_WISHLIST = "WishItemTable";
    private static final String KEY_ID = "id";
    private static final String KEY_CATEGORY = "category";
    private static final String KEY_WEBID = "productid";

    //private static final String KEY_EXAMPLE = "example";
    private static final String KEY_DATE = "date";
    //private static final String KEY_STATUS = "status";
    //private static final String KEY_MARKED = "mark";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        //3rd argument to be passed is CursorFactory instance
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_RECENT_TABLE = "CREATE TABLE " + TABLE_RECENT + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_CATEGORY + " TEXT,"
                + KEY_WEBID +" TEXT," +KEY_DATE + " TEXT"+")";

        db.execSQL(CREATE_RECENT_TABLE);
        String CREATE_WISHLIST_TABLE = "CREATE TABLE " + TABLE_WISHLIST + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_CATEGORY + " TEXT,"
                + KEY_WEBID +" TEXT," +KEY_DATE + " TEXT"+")";

        db.execSQL(CREATE_WISHLIST_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_RECENT);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_WISHLIST);

        // Create tables again
        onCreate(db);
    }

    // code to add the new Wish
   public void addWish(WishItem wishItem) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_DATE,String.valueOf(wishItem.getDate()) ); //
        values.put(KEY_CATEGORY, wishItem.getCategory()); //
        values.put(KEY_WEBID, wishItem.getProductId());

        Log.d("MainActivity", "Database Wish entered  : Success  ");
        // Inserting Row
        db.insert(TABLE_WISHLIST, null, values);
        //2nd argument is String containing nullColumnHack
        db.close(); // Closing database connection
    }
    // code to add the new Recent
   public void addRecent(WishItem wishItem) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_DATE,String.valueOf(wishItem.getDate()) ); //
        values.put(KEY_CATEGORY, wishItem.getCategory()); //
        values.put(KEY_WEBID, wishItem.getProductId());

        Log.d("MainActivity", "Database Recent Item entered  : Success  ");
        // Inserting Row
        db.insert(TABLE_RECENT, null, values);
        //2nd argument is String containing nullColumnHack
        db.close(); // Closing database connection
    }

    // code to get the single Term
   /* Term getTerm(Integer id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_DICT, new String[] { KEY_ID,
                        KEY_WORD,KEY_MEANING,KEY_EXAMPLE,KEY_STATUS,KEY_MARKED,KEY_DATE }, KEY_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        Term term = new Term(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(2),cursor.getString(3),cursor.getString(4),cursor.getString(5),cursor.getString(6));
        Log.d("MainActivity", "Fetching single term with Id :   "+id.toString());
        // return term
        return term;
    }*/



    // code to get all Term in a list view
   /* public List<WishItem> getAllWish(String date) {
        List<WishItem> termList = new ArrayList<WishItem>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_WISHLIST +"WHERE "+KEY_DATE+"= "+date ;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);


        if (cursor != null)
            cursor.moveToFirst();

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Term term = new Term();
                term.setId(Integer.parseInt(cursor.getString(0)));
                term.setWord(cursor.getString(1));
                term.setMeaning(cursor.getString(2));
                term.setExample(cursor.getString(3));
                term.setStatus(cursor.getString(4));
                term.setMarked(cursor.getString(5));
                term.setDate(cursor.getString(6));
                // Adding term to list
                termList.add(term);
            } while (cursor.moveToNext());
        }

        // return story list
        return termList;
    }*/

    // code to get all wish in a list view
    public List<WishItem> getAllWish() {
        List<WishItem> wishList = new ArrayList<WishItem>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_WISHLIST;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                WishItem wish = new WishItem();
                wish.setID(Integer.parseInt(cursor.getString(0)));
                wish.setCategory(cursor.getString(1));
                wish.setProductId(cursor.getString(2));
                wish.setDate(cursor.getString(3));

                // Adding term to list
                wishList.add(wish);


            } while (cursor.moveToNext());
        }Log.d("MainActivity", "Fetching list from database....   ");
        //Log.d("MainActivity", "database gives  :like  "+cursor.getString(4)+"Boolean.valueOf(cursor.getString(4))");
        // return storylist list
        return wishList;
    }

    // code to get all Recently seen in a list view
    public List<WishItem> getAllRecent() {
        List<WishItem> wishList = new ArrayList<WishItem>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_RECENT;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                WishItem wish = new WishItem();
                wish.setID(Integer.parseInt(cursor.getString(0)));
                wish.setCategory(cursor.getString(1));
                wish.setProductId(cursor.getString(2));
                wish.setDate(cursor.getString(3));

                // Adding term to list
                wishList.add(wish);


            } while (cursor.moveToNext());
        }
        Log.d("MainActivity", "Fetching list from database....   ");
        //Log.d("MainActivity", "database gives  :like  "+cursor.getString(4)+"Boolean.valueOf(cursor.getString(4))");
        // return storylist list
        return wishList;
    }

    // code to update the single wishitem
    public int updateWish(WishItem wishItem) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_WEBID, wishItem.getProductId());
        values.put(KEY_CATEGORY, wishItem.getCategory());
        values.put(KEY_DATE, wishItem.getDate());

        // updating row
        return db.update(TABLE_WISHLIST, values, KEY_ID + " = ?",
                new String[] { String.valueOf(wishItem.getID()) });
    }

    // code to update the single Recent Item
    public int updateRecentItem(WishItem wishItem) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_WEBID, wishItem.getProductId());
        values.put(KEY_CATEGORY, wishItem.getCategory());
        values.put(KEY_DATE, wishItem.getDate());

        // updating row
        return db.update(TABLE_RECENT, values, KEY_ID + " = ?",
                new String[] { String.valueOf(wishItem.getID()) });
    }

    // Deleting single wish
    public void deleteWish(WishItem wishItem) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_WISHLIST, KEY_ID + " = ?",
                new String[] { String.valueOf(wishItem.getID()) });
       // db.close();
    }
    public void deleteWish(String Productid) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_WISHLIST, KEY_WEBID + " = ?",
                new String[] { String.valueOf(Productid) });
        // db.close();
    }

    // Deleting single Recent Item
    public void deleteRecentItem(WishItem wishItem) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_RECENT, KEY_ID + " = ?",
                new String[] { String.valueOf(wishItem.getID()) });
        // db.close();
    }

    // Getting RecentItem Count
    public int getRecentCount() {
        String countQuery = "SELECT  * FROM " + TABLE_RECENT;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        //cursor.close();
        //db.close();
        // return count
        return cursor.getCount();
    }
    // Getting wishitems Count
    public int getWishCount() {
        String countQuery = "SELECT  * FROM " + TABLE_WISHLIST;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        //cursor.close();
        //db.close();
        // return count
        return cursor.getCount();
    }

}
