package com.example.aksha.project_doc;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class HomeCategoryFragment extends Fragment {
    View v;
    RecyclerView recyclerView;
    Adapter_Doc_Category adapter_doc_category;
    ArrayList<String> name;
    ArrayList<Integer> imageId;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v=inflater.inflate(R.layout.activity_doctor_category, container, false);

        name = new ArrayList<>();
        //by patel
        name.add("haematologist");
        name.add("medicine");
        name.add("neuro physician");
        name.add("m.d. physician");
        name.add("microbiologist");
        name.add("maternity and child health");
        name.add("neuro surgeon");
        name.add("oncology cancer");
        name.add("nephrologist");
        name.add("neurologist");
        name.add("opthamology");
        name.add("orthopaedic");
        name.add("obct");
        name.add("occupational therapist");
        name.add("paediatrician");

        //by sharma
        name.add("dentist");
        name.add("critical care specialist");
        name.add("cardio thoracic surgeon");
        name.add("cardiology");
        name.add("consultant physician");
        name.add("audiologist");
        name.add("ayurvedic");
        name.add("occupational theraoist");

        imageId = new ArrayList<>();
        imageId.add(R.drawable.haematologist);//clear
        imageId.add(R.drawable.medicine_1);//clear
        imageId.add(R.drawable.neurologist_1);//clear
        imageId.add(R.drawable.physician_1);//clear
        imageId.add(R.drawable.microbiologist);//clear
        imageId.add(R.drawable.gynecologist);//clear
        imageId.add(R.drawable.neurologist_1);//clear
        imageId.add(R.drawable.oncologist);//clear
        imageId.add(R.drawable.nephrology_1);//clear
        imageId.add(R.drawable.neurologist_1);//clear
        imageId.add(R.drawable.ophthalmologist_1);//clear
        imageId.add(R.drawable.orthopedic);//clear
        imageId.add(R.drawable.gastroenterology_1);//clear
        imageId.add(R.drawable.ot_1);//clear
        imageId.add(R.drawable.pediatrician_1);//clear

        //by sharma
        imageId.add(R.drawable.dentist);//clear
        imageId.add(R.drawable.critical_care_1);//clear
        imageId.add(R.drawable.cardiothoraric);//clear
        imageId.add(R.drawable.cardiology);//clear
        imageId.add(R.drawable.physician_1);//clear
        imageId.add(R.drawable.audiologist_1);//clear
        imageId.add(R.drawable.ayurvedic_1);//clear
        imageId.add(R.drawable.therapist_1);//clear

        recyclerView = v.findViewById(R.id.recyclerView);

        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getContext(), 2);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecorator(2, dpToPx(), true));

        adapter_doc_category = new Adapter_Doc_Category(getContext(), name, imageId, new Adapter_Doc_Category.clickListener() {
            @Override
            public void onItemClicked(String docCategory) {
                Intent intent = new Intent(getContext(), displayDoctors.class);
                intent.putExtra("category", docCategory);
                Log.d("DocCategoryClass", "category :" + docCategory);
                startActivity(intent);
            }
        });

        recyclerView.setAdapter(adapter_doc_category);



        return v;

    }


    private int dpToPx() {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, r.getDisplayMetrics()));
    }

    public class GridSpacingItemDecorator extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecorator(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }
}
