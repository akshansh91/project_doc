package com.example.aksha.project_doc;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.aksha.project_doc.utils.ServerConfig;

import java.util.ArrayList;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class hospitalActivity extends AppCompatActivity {
    private TextView hospitalnametextview, hospitaladdresstextview, hospitaldetailtextview, aboutustextview;
    private ImageView hospitalimageview;

    String hospitalName, hospitalAddress, hospitalDetails, responseString, TAG = "displayDoctors";
    /*******/
    RecyclerView hospitaldoctorlist;

    OkHttpClient client;
    HttpUrl.Builder builder;

    Request request;
    Response response;

    ProgressDialog progressDialog;

    ArrayList<String> list;
    ArrayList<ArrayList<String>> finalList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hospital_class);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Log.d(TAG, "hospital-> doctor : activity started");
        Intent intent = getIntent();
        hospitalName = intent.getStringExtra("hospital");
        Log.d(TAG, "hospital : " + hospitalName);
        hospitalAddress = intent.getStringExtra("hospitaladdress");

        hospitaldetailtextview = findViewById(R.id.hospitaldetail1);
        aboutustextview = findViewById(R.id.aboutsustextView1);

        //if details exist about hospital show it otherwise hide it
        if (intent.hasExtra("hospitaldetails")) {
            hospitalDetails = intent.getStringExtra("hospital");
            aboutustextview.setVisibility(View.VISIBLE);
            hospitaldetailtextview.setVisibility(View.VISIBLE);
            hospitaldetailtextview.setText(intent.getStringExtra("hospitaldetails"));
        }

        hospitalnametextview = (TextView) findViewById(R.id.hospitalName1);
        hospitaladdresstextview = (TextView) findViewById(R.id.hospitalAddress1);
        hospitalimageview = (ImageView) findViewById(R.id.hospitalpic1);
        hospitaladdresstextview.setText(hospitalAddress);
        hospitalnametextview.setText(hospitalName);

        finalList = new ArrayList<>();

        hospitaldoctorlist = findViewById(R.id.hospitaldoctorlist1);
        initiateRecycleview();

    }

    //initialie doctor list related to hospital
    private void initiateRecycleview() {


        //hospitaldoctorlist = findViewById(R.id.doctorList);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(hospitalActivity.this, LinearLayoutManager.VERTICAL, false);
        hospitaldoctorlist.setLayoutManager(linearLayoutManager);

        progressDialog = new ProgressDialog(hospitalActivity.this);
        progressDialog.setTitle("Loading");
        progressDialog.setMessage("Please Wait!!");
        progressDialog.setIndeterminate(false);
        // progressDialog.setCancelable(false);

        //get list of doctor from server
        getList(hospitalName);
    }

    @SuppressLint("StaticFieldLeak")
    class getDoctorsDetails extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {

            Log.d(TAG, "inside getDoctorsDetails doInBackground()");

            String hospitalName = strings[0];
            Log.d(TAG, "inside getDoctorsDetails doInBackground() " + hospitalName);

            client = new OkHttpClient.Builder().connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS).build();

            builder = Objects.requireNonNull(HttpUrl.parse(new ServerConfig().getDocListUsingHospital())).newBuilder();
            builder.addQueryParameter("hospital", hospitalName.toLowerCase());/********************/

            String url = builder.build().toString();

            request = new Request.Builder().url(url).build();

            try {
                response = client.newCall(request).execute();
            } catch (Exception e) {
                Log.d(TAG, "inside getDoctorsDetails doInBackground() exe_1: " + e);
            }

            if (response.body() != null) {
                try {
                    responseString = response.body().string();
                } catch (Exception e) {
                    Log.d(TAG, "inside getDoctorsDetails doInBackground() exe_2: " + e);
                }
            }
            Log.d(TAG, "inside getDoctorsDetails doInBackground() responseString: " + responseString);
            return responseString;
        }

        @Override
        protected void onPostExecute(String s) {
            if (!s.equals("error")) {
                progressDialog.dismiss();
                Log.d(TAG, "inside getDoctorsDetails onPostExecute() going to process response");
                processResponse(s);
            } else {
                Toast.makeText(hospitalActivity.this, "SERVER ERROR", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void getList(String hospitalName) {
        //get list from the server. . .
        Log.d(TAG, "inside getList()");
        hospitalActivity.getDoctorsDetails doctorsDetails = new hospitalActivity.getDoctorsDetails();
        doctorsDetails.execute(hospitalName);
    }

    private void processResponse(String s) {
        //adding data in different array lists ...
        String chunkOfData[] = s.split("___");

        int counter = 1;
        int size = chunkOfData.length;

        while (counter < size) {
            list = new ArrayList<>();

            String blockOfData = chunkOfData[counter];
            String[] array = blockOfData.trim().split("\\*", -1);

            //name,degree,phone,address,timingM,timingE,m,tu,wed,th,fr,sat,su,note

            String name = array[0];
            list.add(name);
            Log.d("API_Call", "name: " + name);

            String degree = array[1];
            list.add(degree);
            Log.d("API_Call", "degree: " + degree);

            String phone = array[2];
            list.add(phone);
            Log.d("API_Call", "phone: " + phone);

            String address = array[3];
            list.add(address);
            Log.d("API_Call", "address: " + address);

            String timingM = array[4];
            list.add(timingM);
            Log.d("API_Call", "timingM: " + timingM);

            String timingE = array[5];
            list.add(timingE);
            Log.d("API_Call", "timingE: " + timingE);

            String m = array[6];
            list.add(m);
            Log.d("API_Call", "m: " + m);

            String tu = array[7];
            list.add(tu);
            Log.d("API_Call", "tu: " + tu);

            String wed = array[8];
            list.add(wed);
            Log.d("API_Call", "wed: " + wed);

            String th = array[9];
            list.add(th);
            Log.d("API_Call", "th: " + th);

            String fr = array[10];
            list.add(fr);
            Log.d("API_Call", "fr: " + fr);

            String sat = array[11];
            list.add(sat);
            Log.d("API_Call", "sat: " + sat);

            String sun = array[12];
            list.add(sun);
            Log.d("API_Call", "sun: " + sun);

            /*String fees = array[13];
            list.add(fees);
            Log.d("API_Call", "fees: " + fees);*/

            String note = array[13];
            list.add(note);
            Log.d("API_Call", "note: " + note);

            String id = array[14];
            list.add(id);

            Log.d("API_Call", "List" + counter + ":" + list);
            finalList.add(list);
            counter++;

        }

        Adapter_HospitalDocList adapterDocList = new Adapter_HospitalDocList(hospitalActivity.this, finalList, new Adapter_HospitalDocList.clickListener() {
            @Override
            public void onItemClicked(String id) {
                Intent intent = new Intent(hospitalActivity.this, viewDoctorProfile.class);
                intent.putExtra("id", id);
                Log.d("hospitalActivity", "id :" + id);
                startActivity(intent);
            }
        });
        hospitaldoctorlist.setAdapter(adapterDocList);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            Log.v("MainActivity", "Backpress button is clicked ");
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
