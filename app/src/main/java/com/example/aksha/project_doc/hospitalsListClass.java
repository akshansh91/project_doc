package com.example.aksha.project_doc;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;

public class hospitalsListClass extends AppCompatActivity implements Adapter_HospitalList.hospitalClickListener {

    RecyclerView hospitalList;
    LinearLayoutManager linearLayoutManager;
    Adapter_HospitalList adapterHospitalList;

    ArrayList<String> hospitalName;
    ArrayList<String> hospitalAddress;
    String docHospital, docHospitaladdress, getDocHospitaldetails, TAG = "HospitalList";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hospitalslist_class);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        hospitalName = new ArrayList<>();
        hospitalAddress = new ArrayList<>();

        hospitalName.add("Victoria Hospital");
        hospitalName.add("Marble City Hospital");
        hospitalName.add("Shri Jagdish Hospital");
        hospitalName.add("mannulal hospital");
        hospitalName.add("National Hospital");

        //hospitalName.add("Laxmi Narayan Hospital");

        hospitalName.add("Motilal Nehru Hospital");
        hospitalName.add("Jamdar Hospital");
        hospitalName.add("Jabalpur Hospital");
        hospitalName.add("grover heart and kumar hospital");

        hospitalName.add("Grovert Heart Hospital"); //
        hospitalName.add("Medical College Hospital");//
        hospitalName.add("MPEB Hospital");
        hospitalName.add("GOVT. AYURVEDIC COLLEGE");
        hospitalName.add("Railway Hospital");

        hospitalName.add("Anant Hospital");
        // hospitalName.add("chhowra hospital");//Chhabra Hospital
        hospitalName.add("J.K. Hospital");
        hospitalName.add("Mahakaushal Hospital");
        hospitalName.add("Ashish Hospital");

        hospitalName.add("Bhandari Hospital");
        hospitalName.add("Shree Narmada Hospital");
        hospitalName.add("Suvidha Hospital");
        hospitalName.add("Maa Kripa Cancer Hospital");
        //hospitalName.add("bhatolia Hospital");//Batalia Hospital

        // hospitalName.add("prakash cancer hospital");// not found
        hospitalName.add("Samadhan Hospital");
        hospitalName.add("Triveni Hospital");
        hospitalName.add("Central Hospital");
        hospitalName.add("Sanjivan Hospital");
        hospitalName.add("Sonal Nursing Home");
        //hospitalName.add("Makharjee Hospital");//D.R Mukherji P. G. Hospital  //Mukherjee Hospital

        hospitalAddress.add("Badi Omti, Ganjipura Road, Ganjipura, Jabalpur, Madhya Pradesh 482002");
        hospitalAddress.add("No. 321, Parsi Anjuman Complex, Bhanwartal Extension, Near Museum, Napier Town, Jabalpur, Madhya Pradesh 482001");
        hospitalAddress.add(" Opp Railway Police Ground , Jabalpur-482001");
        hospitalAddress.add("Dixitpura Road, Uprainganj, Jabalpur, Madhya Pradesh 482002");
        hospitalAddress.add("Wright Town, Jabalpur, Madhya Pradesh 482002");
        //hospitalAddress.add("1125, Madan Mahal, Nagpur Road, Jabalpur, Madhya Pradesh 482001");
        hospitalAddress.add("1125, Madan Mahal, Nagpur Road, Jabalpur, Madhya Pradesh 482001");
        hospitalAddress.add("816,golbazar, Jabalpur, Madhya Pradesh 482001");
        hospitalAddress.add("Russel Crossing, Napier Town, Jabalpur, Madhya Pradesh 482002");
        hospitalAddress.add("Madan Mahal Railway Station Rd, Wright Town, Jabalpur, Madhya Pradesh 482002");

        hospitalAddress.add("Madan Mahal Railway Station Rd, Wright Town, Jabalpur, Madhya Pradesh 482002");  //
        hospitalAddress.add("NSCB Medical College Hospital, Jabalpur - 482001");//
        hospitalAddress.add("MPSEB Colony, Shakti Nagar, Jabalpur, Madhya Pradesh 482008");
        hospitalAddress.add("Gwarighat near geetagham , Jabalpur ,Madhya Pradesh 482008");
        hospitalAddress.add("Railway Station Road, Indra Market, Jabalpur, Madhya Pradesh 482001");
        hospitalAddress.add("Madan Mahal, Station Road,Near Gate No 4, Wright Town, Jabalpur ,Madhya Pradesh 482002");
        //hospitalAddress.add("");//66, Bharipur Rd, Market, Bharipur, Galgala, Jabalpur, Madhya Pradesh 482002
        hospitalAddress.add("No. 2006, 2nd Lane, Wright Town, Jabalpur, Madhya Pradesh 482002");
        hospitalAddress.add("Gate No. 3, Gau Mata Chowk Opp Nehru Stadium, Wright Town, Jabalpur, Madhya Pradesh 482002");
        hospitalAddress.add("Home Science College Road, Napier Town, Jabalpur, Madhya Pradesh 482002");

        hospitalAddress.add("RB-II 431/3, NH30, Howbag Railway Colony, Nehru Railway Colony, Gorakhpur, Jabalpur, Madhya Pradesh 482001");
        hospitalAddress.add(" Shatabdipuram, M R -4 Road, Corporate Block-D , Jabalpur-482002");
        hospitalAddress.add("230, Nagpur Rd, Choti line phatak, Jabalpur, Madhya Pradesh 482001");
        hospitalAddress.add("911, Naiper Town, Garha Road, Jabalpur, Madhya Pradesh 482002");
        //hospitalAddress.add("");//Naya Mohalla, Jabalpur, Madhya Pradesh 482001
        //hospitalAddress.add("");//
        hospitalAddress.add("1625, Ima Hall Road, Wright Town, Jabalpur, Madhya Pradesh 482002");
        hospitalAddress.add("816,golbazar, Jabalpur, Madhya Pradesh 482001");
        hospitalAddress.add("Sneh Nagar, Jabalpur, Madhya Pradesh 482002");
        hospitalAddress.add("Ramnagar, Adhartal, Jabalpur, Madhya Pradesh 482004");
        hospitalAddress.add("Near Tayebali Chowk, South Civil Lines, Jabalpur, Madhya Pradesh 482001");
        //hospitalAddress.add("");//Near Prem Mandir 1933, Wright Town, Jabalpur, Madhya Pradesh 482002    //MP SH 22, Mohan Vihar Colony, Sanjeevani Nagar, Jabalpur, Madhya Pradesh 482003

        hospitalList = findViewById(R.id.hospitalList);
        linearLayoutManager = new LinearLayoutManager(hospitalsListClass.this,
                LinearLayoutManager.VERTICAL, false);
        hospitalList.setLayoutManager(linearLayoutManager);

        Log.d("HospitalList", "name * address " + hospitalName.size() + " * " + hospitalAddress.size());

        adapterHospitalList = new Adapter_HospitalList(hospitalsListClass.this, hospitalName, hospitalAddress, this);
        hospitalList.setAdapter(adapterHospitalList);
    }

    @Override
    public void hospitalItemClicked(View v, int pos) {
        docHospital = hospitalName.get(pos);
        docHospitaladdress = hospitalAddress.get(pos);

        Intent intent = new Intent(hospitalsListClass.this, hospitalActivity.class);
        intent.putExtra("hospital", docHospital);
        intent.putExtra("hospitaladdress", docHospitaladdress);
        //intent.putExtra("hospitaldetails", docHospitaldetails);
        Log.d(TAG, "hospitalname : address" + docHospital + " -> " + docHospitaladdress);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            Log.v("MainActivity", "Backpress button is clicked ");
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
